<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Proposta_Model extends Base_Model
{
    public $id;
    public $cliente_nome;
    public $cliente_email;
    public $assunto;
    public $cod_imoveis;
    public $id_corretor;
    public $data;

    protected $table = 'tb_propostas';

    public function novo(Proposta_Model $proposta)
    {
        $this->db->insert($this->table, $proposta);
        return $this->db->insert_id();
    }

    public function total_por_corretor($id_corretor)
    {
        return $this->db->where('id_corretor', $id_corretor)->count_all_results($this->table);
    }

    public function pelo_corretor($id_corretor)
    {
        return $this->db->where('id_corretor', $id_corretor)->get($this->table)->result();
    }

    public function pelo_cliente($email)
    {
        return $this->db->where('cliente_email', $email)->get($this->table)->result();
    }
}