<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Contato_Imovel_Interesse_Model extends Base_Model
{
    public $id;
    public $nome;
    public $telefone;
    public $email;
    public $cod_imovel;
    public $id_corretor;
    public $obs;
    public $criado_em;

    protected $table = 'tb_contato_imovel_interesse';
}