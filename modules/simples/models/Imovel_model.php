<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Empreendimento_model.php');
require_once(__DIR__ . '/../libraries/Finalidades.php');

class Imovel_Model extends Empreendimento_Model
{
    protected $table = 'view_imovel';
    protected $tb_tipo = 'tb_imovel_tipo';
    protected $tb_foto = 'tb_imovel_foto';
    public $tb_foto_foreign_key = 'id_imovel';
    public $tb_video = 'tb_imovel_video';
    public $tb_video_foreign_key = 'id_imovel';
    protected $tb_empreendimento_complemento = 'tb_imovel_complemento';
    protected $tb_complemento_foreign_key = 'id_imovel';
    //protected $tb_complemento = 'tb_complemento';

    protected $filtro_sql_condition = array(
        'id_tipo' => 'where_in'
    );

    public function todos_complementos()
    {
        return $this->db
            ->order_by('tipo')
            ->order_by('complemento')
            ->get($this->tb_complemento)
            ->result();
    }

    public function cidades_imoveis()
    {
        return $this->db->query('SELECT distinct(cidade) FROM ' . $this->table . ' ORDER BY cidade ASC')->result();
    }

    public function tipos()
    {
        return $this->db->get($this->tb_tipo)->result();
    }

    public function pesquisar(array $filtro, $pagina, $limite, $order_by = NULL)
    {
        $this->select_foto_principal();

        $this->monta_filtro($filtro);

        if(! is_null($order_by))
            $this->db->order_by($order_by);

        return $this->db->limit($limite, ($pagina * $limite))->get($this->table)->result();
    }

    public function pesquisar_total_resultados(array $filtro)
    {
        $this->monta_filtro($filtro);

        return $this->db->from($this->table)->count_all_results();
    }

    private function monta_filtro(array $filtro)
    {
        $valoresCond = array();
        if(isset($filtro['preco_min']) && $filtro['preco_min'] > 0)
            $valoresCond['valor >='] = (int)$filtro['preco_min'];
        if(isset($filtro['preco_max']) && $filtro['preco_max'] > 0 && $filtro['preco_max'] != 3000000)
            $valoresCond['valor <='] = (int)$filtro['preco_max'];

        unset($filtro['preco_min']);
        unset($filtro['preco_max']);

        $filtro = array_filter($filtro);

        foreach ($filtro as $key => $val)
            if (is_numeric($key)) // only numbers, a point and an `e` like in 1.1e10
                unset($filtro[$key]);

        if(isset($filtro['complementos']))
        {
            if(is_array($filtro['complementos']))
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento IN (" . implode(', ', $filtro['complementos']) . ")");
            else
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento = " . $filtro['complementos']);

            unset($filtro['complementos']);
        }

        if(count($valoresCond) > 0)
            $this->db->where($valoresCond);

        $pesqConfigs = $this->config->item('pesquisa');

        foreach ($filtro as $key => $value)
        {
            if(isset($this->filtro_sql_condition[$key]))
            {
                $function = $this->filtro_sql_condition[$key];
                $this->db->$function($key, $value);
            }
            else if(is_array($value))
                $this->db->where_in($key, $value);
            else if(isset($pesqConfigs[$key]) && $pesqConfigs[$key] == $value)
                $this->db->where($key . ' >=', $value);
            else
                $this->db->like($key, $value);
        }

        $this->db->select("$this->table.*");
    }

    /**
     * @param $imovel
     * @param $limit
     * @return mixed
     */
    public function similares($imovel, $limit, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        $this->db->order_by('RAND()')
            ->where('id !=', $imovel->id)
            ->where('id_tipo', $imovel->id_tipo);

        if( ! is_null($imovel->id_condominio))
            $this->db->or_where('id_condominio', $imovel->id_condominio);

        $config = $this->config->item('pesquisa');

        if( isset($config['similares_valores'][$imovel->finalidade]) )
        {
            $porcentagem = $config['similares_valores'][$imovel->finalidade];

            if($imovel->finalidade == Finalidades::Venda)
                $this->valores_similares($imovel->valor, $porcentagem);
            else if($imovel->finalidade == Finalidades::Aluguel)
                $this->valores_similares($imovel->valor_aluguel_mensal, $porcentagem);
            else
                $this->valores_similares($imovel->valor_aluguel_diario, $porcentagem);
        }

        return $this->db
            ->limit($limit)
            ->get($this->table)
            ->result();
    }

    protected function valores_similares($imovel_valor, $porcentagem)
    {
        $valor = (($imovel_valor * $porcentagem) / 100);

        $valor_maximo = $imovel_valor + $valor;
        $valor_minimo = $imovel_valor - $valor;

        if($imovel_valor > 0)
        {
            $this->db->where('valor <=', $valor_maximo);

            if($valor_minimo > 0)
            {
                $this->db->where('valor >=', $valor_minimo);
            }
        }
    }

    public function pelo_condominio($id_condominio, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db
            ->where('id_condominio', $id_condominio)
            ->get($this->table)
            ->result();
    }

    public function formatar_dados()
    {
        $this->table = 'imoveis';

        //APENAS OS ATIVOS
        $this->db->where('ativo !=', 'ativo');
        $imoveis = $this->listar_todos();

        echo 'imóveis total: ' . count($imoveis);
        echo '<br>';
        echo '<br>';

        $linhas_afetadas = 0;

        //var_dump($imoveis[0]->itens); die;

        foreach($imoveis as $imovel)
        {
            $obj = unserialize(utf8_decode($imovel->itens));

            if($obj == '')
                continue;

            $itens = new stdClass();
            foreach($obj as $key => $valor)
            {
                if(strlen($valor) == 0)
                    $valor = 0;
                else
                    $valor = (int)$valor;

                if(strpos($key, 'Dorm') !== false)
                    $itens->dormitorios = $valor;
                else if(strpos($key, 'Garagem') !== false)
                    $itens->garagem = $valor;
                else if(strpos($key, 'Banheiros') !== false)
                    $itens->banheiros = $valor;
                else if(strpos($key, 'Su') !== false)
                    $itens->suites = $valor;
            }

            $linhas_afetadas += $this->atualizar($imovel->id, $itens, 'tb_imovel_lixeira');
        }

        echo '<br>';
        echo 'linhas afetadas: ' . $linhas_afetadas;

        //var_dump(explode('|', $imoveis[0]->image)); die;

        /*$fotos = [];
        foreach($imoveis as $imovel)
        {
            foreach(explode('|', $imovel->image) as $foto)
            {
                $registro = new stdClass();
                $registro->id_imovel = $imovel->id;
                $registro->arquivo = $foto;
                $registro->tipo = 1;
                $registro->data = date('Y-m-d H:i:s');

                $fotos[$imovel->id][] = $registro;
            }
            var_dump($fotos[$imovel->id]);
            echo '<br>';
            echo 'inserte linhas afetadas para [' . $imovel->id . ']: ' .$this->inserir_varios($fotos[$imovel->id], 'tb_imovel_lixeira_foto');
            echo '<br>';
            echo '<br>';
        }

        return $imoveis;*/

    }

    public function adicionar_imoveis_novos()
    {
        //$imovel = new stdClass();

        //die('asd');

        $this->table = 'tb_imovel';
        $resultados = array();
        foreach($this->imoveis as $imovel)
        {
            $imov_delete = $this->db->where('codigo_referencia', $imovel['ci'])->get('tb_imovel')->first_row();

            if( ! is_null($imov_delete))
            {
                $this->db->where('id_imovel', $imov_delete->id)
                        ->delete('tb_imovel_foto');

                $this->db->where('id', $imov_delete->id)
                        ->delete($this->table);
            }


            $imov = new stdClass();
            $imov->codigo_referencia = $imovel['ci'];
            $imov->finalidade = 1;
            $imov->id_tipo = $imovel['tipo'];
            $imov->descricao = $imovel['descricao'];
            $imov->estado = $imovel['estado'];
            $imov->pais = 'Brasil';
            $imov->bairro = $imovel['bairro'];
            $imov->cidade = $imovel['local'];
            $imov->rua = $imovel['endereco'];
            $imov->valor = $imovel['valor'];
            $imov->valor_mostrar_site = 1;
            $imov->data_cadastro = $imovel['data'];

            $imov->id = $this->inserir($imov);

            $fotos = array();
            foreach(explode('|', $imovel['image']) as $foto)
            {
                $registro = new stdClass();
                $registro->id_imovel = $imov->id;
                $registro->arquivo = $foto;
                $registro->tipo = 1;
                $registro->data = date('Y-m-d H:i:s');
                $registro->mostrar_site = 1;

                $fotos[] = $registro;
            }

            if(count($fotos) > 0)
            {
                $resultados[$imov->id][] = count($fotos) == $this->inserir_varios($fotos, 'tb_imovel_foto');
            }
            else
                $resultados[$imov->id] = 'Sem fotos';
        }

        var_dump($resultados);
    }



protected $imoveis = array(
array('id' => '5784','ci' => '2752','tipo' => '13','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Av. Juscelino Kubitschek, 594','bairro' => 'Presidente','estado' => 'RS','distancia' => '','descricao' => 'Geminado 3 unidades, 58m², 2 dormitórios, 1 banheiro, sala, cozinha, pé direito 4,30m p/ mezanino.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5733','valor' => '130000.00','image' => '1481221079_WhatsApp_Image_20161208_at_144853jpeg.jpg|1481221040_WhatsApp_Image_20161208_at_144850jpeg.jpg|1481221042_WhatsApp_Image_20161208_at_144851jpeg.jpg|1481221043_WhatsApp_Image_20161208_at_144852jpeg.jpg|1481221043_WhatsApp_Image_20161208_at_144854_1jpeg.jpg|1481221044_WhatsApp_Image_20161208_at_144855_1jpeg.jpg|1481221045_WhatsApp_Image_20161208_at_144855jpeg.jpg|1481221045_WhatsApp_Image_20161208_at_144856_1jpeg.jpg|1481221046_WhatsApp_Image_20161208_at_144856jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'aceita terreno no centro, ou carro, pode financiar pela caixa, com habite-se, parcela direto com entrada de 30%.','chaves' => '','data' => '2016-12-07','dataUpdate' => '2016-12-07'),
array('id' => '5785','ci' => '2753','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Rolante 1490','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa com 3 dormitórios sendo 1 suíte, 3 banheiros, cozinha e estar conjugados, garagem para 1 carro.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5734','valor' => '380000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Quer dinheiro, esta alugada no verão até fevereiro de 2017.','chaves' => '','data' => '2016-12-07','dataUpdate' => '2016-12-07'),
array('id' => '5787','ci' => '2754','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Rolante 1596','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa com 2 dormitórios, 1 banheiro, sala e cozinha conjugados. Anexo com 1 dormitório e 1 banheiro. Garagem 2 carros.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5735','valor' => '340000.00','image' => '1481222074_rolante_1596_17.jpg|1481221798_rolante_1596_2.jpg|1481221801_rolante_1596_3.jpg|1481221804_rolante_1596_4.jpg|1481221805_rolante_1596_5.jpg|1481221807_rolante_1596_6.jpg|1481221809_rolante_1596_7.jpg|1481221811_rolante_1596_8.jpg|1481221813_rolante_1596_12.jpg|1481221815_rolante_1596_15.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Aceita terreno só próximo a praia. Terreno 190, Casa 310.

Se quiser ficar com terreno + casa R$500 mil livre','chaves' => 'Cindapa- autorizado.','data' => '2016-12-07','dataUpdate' => '2016-12-07'),
array('id' => '5790','ci' => '2755','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Av. Fernando Amaral 1038/ ED. Colombia','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento de frente para avenida, 3 suítes, sala, cozinha, lavanderia, um box para carro.
','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5736','valor' => '594000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => ' Apto 904, ','chaves' => 'Sindica Marlene 99777927','data' => '2016-12-07','dataUpdate' => '2016-12-07'),
array('id' => '5792','ci' => '2756','tipo' => '11','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Taquara x Fundos Rolante 1596','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => '','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5738','valor' => '206000.00','image' => '1481221995_rolante_1596_15.jpg|1481221997_rolante_1596_18.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Terreno 13x30, cercado,posição frente mar.','chaves' => '','data' => '2016-12-07','dataUpdate' => '2016-12-07'),
array('id' => '5795','ci' => '2757','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Tupanciretã','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa com 2 dormitórios, sala e cozinha amplas, garagem fechada.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilsandro','id_cliente' => '5739','valor' => '230000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Tupaciretã passando a Carazinho.','chaves' => '','data' => '2016-12-07','dataUpdate' => '2016-12-07'),
array('id' => '5801','ci' => '2758','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Av. Fernando Amaral 1038/ ED. Colombia APTO 1104','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento com 3 suítes, sala de jantar, estar, lavanderia, 1 banheiro, 2 box e depósito.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5742','valor' => '750000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-08','dataUpdate' => '2016-12-08'),
array('id' => '5800','ci' => '2759','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Fernando Amaral 1038, APTO 1302','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento 4 dormitórios, sala, cozinha, banheiro social, 1 box.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5741','valor' => '900000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'Zeladora','data' => '2016-12-08','dataUpdate' => '2016-12-08'),
array('id' => '5796','ci' => '2760','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Garibalde 1293 esq. Uruguaiana','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa 165m², 3 dormitórios, 3 banheiros, sala, cozinha garagem para 3 carros, com laje. 2 Terrenos (24x30).','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'mariana','id_cliente' => '5740','valor' => '432000.00','image' => '1481285368_garibaldi_1293_1.jpg|1481285370_garibaldi_1293_2.jpg|1481285372_garibaldi_1293_5.jpg|1481285374_garibaldi_1293_6.jpg|1481285375_garibaldi_1293_7.jpg|1481285377_garibaldi_1293_8.jpg|1481285379_garibaldi_1293_10.jpg|1481285381_garibaldi_1293_11.jpg|1481285382_garibaldi_1293_12.jpg|1481285384_garibaldi_1293_15.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-08','dataUpdate' => '2016-12-08'),
array('id' => '5799','ci' => '2761','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'São João 1776 AP 404','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento no centro de Tramandaí com 2 dormitórios, sala, cozinha, churrasqueira, mobiliado e decorado, ar condicionado, piso em porcelanato, aberturas em PVC, elevador e box de garagem.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Diogo','id_cliente' => '4634','valor' => '335000.00','image' => '1481220854_WhatsApp_Image_20161208_at_144106jpeg.jpg|1481220855_WhatsApp_Image_20161208_at_144107jpeg.jpg|1481220855_WhatsApp_Image_20161208_at_144108_1jpeg.jpg|1481220856_WhatsApp_Image_20161208_at_144108jpeg.jpg|1481220857_WhatsApp_Image_20161208_at_144110_1jpeg.jpg|1481220857_WhatsApp_Image_20161208_at_144110_2jpeg.jpg|1481220858_WhatsApp_Image_20161208_at_144110jpeg.jpg|1481220858_WhatsApp_Image_20161208_at_144111jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-08','dataUpdate' => '2016-12-08'),
array('id' => '5803','ci' => '2762','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Rolante 1607','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Sobrado  aproximadamente 210 m², piso superior 3 dormitórios sendo 1 suíte, 2 banheiros, sala estar, salão de festas com churrasqueira. Primeiro piso 1 dormitório, 1 banheiro, sala de estar com lareira, cozinha, garagem com churrasqueira.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5744','valor' => '380000.00','image' => '1481299341_rolante_1607.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'aceita carro 10% tabela fipe, grana.','chaves' => 'Zabka autorizado','data' => '2016-12-09','dataUpdate' => '2016-12-09'),
array('id' => '5814','ci' => '2763','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Encantado 837','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Requinte e conforto, próximo a tudo o que você precisa! Apartamentos de 2 e 3 dormitórios, espaços amplos e ambientes integrados, garantindo todo conforto que você merece. O residencial Elase conta também com belíssimo e amplo hall de entrada totalmente em porcelanato e teto rebaixado em gesso.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5750','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5815','ci' => '2763','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Encantado 837','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Requinte e conforto, próximo a tudo o que você precisa! Apartamentos de 2 e 3 dormitórios, espaços amplos e ambientes integrados, garantindo todo conforto que você merece. O residencial Elase conta também com belíssimo e amplo hall de entrada totalmente em porcelanato e teto rebaixado em gesso.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5750','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5813','ci' => '2764','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Emancipação 350 esq. Jorge Sperb- Apto 32','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento 81m², 3 dormitórios, sala estar, sala jantar, cozinha, banheiro e sacada. Sem mobília, apartamento de frente para avenida.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5749','valor' => '208000.00','image' => '','destaque' => '','tempo' => '','ativo' => 'vendido','lancamento' => '','etiqueta' => '','chaves' => 'Pinheiro- Emancipação 791','data' => '2016-12-23','dataUpdate' => '2016-12-12'),
array('id' => '5812','ci' => '2765','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Tupanciretã 280- Residencial Deise','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Localizado em área nobre de Capão da Canoa, a poucos metros do mar, junto à praia de Atlântida Sul, piscina aquecida, espaço gourmet mobiliado e climatizado, academia equipada com vista para o mar, sala de jogos, hall social mobiliado, entre outras comodidades. Dois elevadores, central de gás, hidrômetro individual, piso em porcelanato. Venha conhecer as unidades de 1, 2 e 3 dormitórios.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5748','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5811','ci' => '2766','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Maramba 2727- ED. Residencial Ohana','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento 139m², 3 dormitórios sendo 1 suíte, banheiro social, estar e jantar integrados, cozinha americana, área de serviço, sacada de frente com churrasqueira.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '2588','valor' => '640000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Opção 304 R$ 490,000','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5810','ci' => '2767','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Av. Sepe 3239- Residence Maui','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamentos com opções de 1, 2 e 3 dormitórios, a partir de 48m², à 200 m do mar. O Maui Residence será entregue com completo sistema que reduz consumo através do uso de sistema inteligentes de áreas verdes. Espaço Kids e espaço gourmet. Piscina panorâmica com vista infinita no terraço.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5722','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5809','ci' => '2768','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Av. Guaraci 3043, Ed. London','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'London com seu estilo, unidades com pátio, piscina panorâmica, no terraço, espaço gourmet integrado com piscina, chimarrodromo, telhado verde, PVB temático equipado e decorado estilo londrino. Pé direito elevado.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5722','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5808','ci' => '2769','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Moacir, 2959 /Ponce de Leon','bairro' => ' ','estado' => 'RS','distancia' => '','descricao' => 'Empreendimento com apartamentos de 2 e 3 dormitórios, localizado junto a praça Tiaraju e avenida da Santinha, fachada 100% revestida em plaquetas, dois elevadores digitais, esquadrias externas em alumínio, piso em porcelanato, espera para água quente, gás central, espera para split, hidrometro individual de água quente, localização nobre e com condição excelente para negociação.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '2588','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5807','ci' => '2770','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Av. Central 1515 Xangri-lá- Ed. Essence','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Empreendimento de alto padrão, apartamentos de 2 e 3 dormitórios com suíte. Infraestrutura de condomínio fechado, construtora líder em empreendimentos de luxo no litoral norte. Portaria 24 horas com guarita blindada, bicicletário, Play Ground, brinquedoteca, salão de festas, sunset lounge, gourmeteria, fitnes, piscina adulto e infantil, prainha, sauna seca - SPA, piscina térmica, sala de jogos.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5747','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5804','ci' => '2771','tipo' => '7','modalidade' => 'Venda','local' => 'Rainha do Mar','endereco' => 'Garassois, 560','bairro' => 'CENTRO','estado' => 'RS','distancia' => '','descricao' => 'Excelente casa 3 dormitórios, 2 banheiros, mobiliada e decorada, garagem com churrasqueira e espaço gourmet.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5745','valor' => '230000.00','image' => '1481631664_13978150_1028123827295088_283976636_o.jpg|1481631612_13884500_1028124210628383_1489031518_n.jpg|1481631615_13918768_1028123843961753_797353470_o.jpg|1481631616_13932264_1028123860628418_1700241516_o.jpg|1481631616_13941042_1028124270628377_1081802352_n.jpg|1481631617_13942271_1028124380628366_1338547708_n.jpg|1481631618_13978012_1028123877295083_857830369_o.jpg|1481631619_13987290_1028123883961749_777559088_o.jpg|1481631620_13987721_1028123840628420_1790814253_o.jpg|1481631621_13987979_1028123833961754_926786371_o.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'Imob Hahn Rainha','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5805','ci' => '2772','tipo' => '9','modalidade' => 'Venda','local' => 'Rainha do Mar','endereco' => 'Rua Marfin, 706','bairro' => 'CENTRO','estado' => 'RS','distancia' => '','descricao' => '1 dormitório, sala, cozinha, banheiro social, salão de festa, enfrente a praça.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5746','valor' => '107000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5806','ci' => '2773','tipo' => '7','modalidade' => 'Venda','local' => 'Xangri-la','endereco' => 'Rua Luiz Comuneli,185','bairro' => ' ','estado' => 'RS','distancia' => '','descricao' => 'Casa com 1 dormitório, sala, cozinha, banheiro social, mobiliada.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5732','valor' => '173000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-12','dataUpdate' => '2016-12-12'),
array('id' => '5816','ci' => '2774','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Graraci esquina Rua Tiaraju','bairro' => ' ','estado' => 'RS','distancia' => '','descricao' => 'Apartamento de 2 e 3 dormitórios com suíte, sala, cozinha, salão de festas, piscina aquecida, sala de jogos, espaço gourmet, garagem, 2 elevadores, vista eterna para o mar.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5751','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-13','dataUpdate' => '2016-12-13'),
array('id' => '5817','ci' => '2775','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Encantado 1023','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento Novo com 2 dormitórios sendo 1 suíte, sala de estar, jantar, área de serviço, churrasqueira, apartamento de frente, no 5º andar, box de garagem no térreo, acabamento classe AA, prédionovo, exelente localização, 3 quadras do mar, próximo ao centro.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5750','valor' => '378000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-13','dataUpdate' => '2016-12-13'),
array('id' => '5818','ci' => '2776','tipo' => '9','modalidade' => 'Venda','local' => 'Capão da Canoa','endereco' => 'Rua Moacir 2252 Esq. Rua Andira- Palazzo Bella Vita','bairro' => ' ','estado' => 'RS','distancia' => '','descricao' => 'Apartamento medindo 113,35m²,  2 dormitórios sendo 1 suíte, 2 banheiros, sala, cozinha, ótima orientação solar, andar baixo, sacada de frente, com churrasqueira, 1 box de garagem, prédio com 2 elevadores e salão de festas.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5751','valor' => '0.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-13','dataUpdate' => '2016-12-13'),
array('id' => '5820','ci' => '2777','tipo' => '11','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua 10','bairro' => 'Emboaba','estado' => 'RS','distancia' => '','descricao' => 'Excelente oportunidade, 3 Lotes medindo 12,5x25m cada. Desconto especial para compra dos 3 lotes.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5752','valor' => '55000.00','image' => '','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => 'Quadra 44, Se vender os 3 lotes aceita carro de até 20% do valor. Por placa.','chaves' => '','data' => '2016-12-13','dataUpdate' => '2016-12-13'),
array('id' => '5821','ci' => '2778','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'São Miguel 886','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Sobrado 5 dormitórios, 1 suíte, 2 banheiros, sala de estar e jantar, cozinha.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5753','valor' => '500000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-13','dataUpdate' => '2016-12-13'),
array('id' => '5823','ci' => '2779','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Sobradinho 1572','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Imóvel diferenciado! Casa com 4 dormitórios, sendo 2 suítes, terraço, sala e cozinha estilo americana, banho social, área de serviço, ampla sala de estar, piso porcelanato, acabamento em gesso. Imóvel finamente mobiliado, sistema de água quente, portão eletrônico, calçada com pedra ferro miracema, região mais valorizada da cidade, 50 m da avenida principal, terreno medindo 13x30.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5754','valor' => '750000.00','image' => '1481659522_WhatsApp_Image_20161210_at_115339_2jpeg.jpg|1481659524_WhatsApp_Image_20161210_at_115342jpeg.jpg|1481659524_WhatsApp_Image_20161210_at_115344jpeg.jpg|1481659525_WhatsApp_Image_20161210_at_115348jpeg.jpg|1481659525_WhatsApp_Image_20161210_at_115350jpeg.jpg|1481659526_WhatsApp_Image_20161210_at_115402_1jpeg.jpg|1481659527_WhatsApp_Image_20161210_at_115402jpeg.jpg|1481659527_WhatsApp_Image_20161210_at_115409jpeg.jpg|1481659528_WhatsApp_Image_20161210_at_115417jpeg.jpg|1481659528_WhatsApp_Image_20161210_at_115421jpeg.jpg','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => 'estuda parcelamento, troca por apartamento na planta.','chaves' => 'Ligar','data' => '2016-12-13','dataUpdate' => '2016-12-13'),
array('id' => '5824','ci' => '2780','tipo' => '11','modalidade' => 'Venda','local' => 'Osório','endereco' => 'Lagoa do Passo','bairro' => 'atlantida sul','estado' => 'RS','distancia' => '','descricao' => 'Terreno em condomínio fechado as margens da estrada do mar, e fundos para a lagoa. Medidas 24x31,5m. Área de 756m².','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5628','valor' => '350000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Estuda parcelamento. Lote 15, Quadra 3.','chaves' => '','data' => '2016-12-14','dataUpdate' => '2016-12-14'),
array('id' => '5825','ci' => '2781','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Ubatuba de Farias esq. com 3 de Outubro. Ed. Lívia','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento no 5º andar, com 2 dormitórios, 1 suíte, sala de estar e jantar integradas, cozinha americana, lavabo, área de serviço, aquecimento de água à gás, aberturas de pvc no estilo porta janela, inclusive nos quartos, sala com sanca decorativa em gesso. Aproximadamente 87m² no apartamento mais terraço com cerca de 90m², 1 vaga de garagem, mobiliado.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5118','valor' => '450000.00','image' => '1481735280_1473363109_WhatsApp_Image_20160908_at_160639jpeg.jpg|1481735256_1473362924_WhatsApp_Image_20160908_at_160722jpeg.jpg|1481735256_1473362937_WhatsApp_Image_20160908_at_160717jpeg.jpg|1481735256_1473362951_WhatsApp_Image_20160908_at_160713jpeg.jpg|1481735257_1473362967_WhatsApp_Image_20160908_at_160642_1jpeg.jpg|1481735257_1473362974_WhatsApp_Image_20160908_at_160643jpeg.jpg|1481735258_1473362985_WhatsApp_Image_20160908_at_160710jpeg.jpg|1481735258_1473362995_WhatsApp_Image_20160908_at_160645_1jpeg.jpg|1481735258_1473363020_WhatsApp_Image_20160908_at_160727jpeg.jpg|1481735259_1473363036_WhatsApp_Image_20160908_at_160640jpeg.jpg','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => 'Estuda Tudo. Ap 504','chaves' => '','data' => '2016-12-14','dataUpdate' => '2016-12-14'),
array('id' => '5826','ci' => '2782','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'São Miguel 1807','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa 4 dormitórios sendo 1 suíte, 4 banheiros, sala jantar e estar, cozinha com churrasqueira.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5755','valor' => '400000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'Cindapa','data' => '2016-12-14','dataUpdate' => '2016-12-14'),
array('id' => '5827','ci' => '2783','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Julio de castilhos 1680','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Sobrado de aproximadamente 100m², 2 suítes, 1 lavabo, cozinha integrada a sala de estar, área de serviço com churrasqueira.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5756','valor' => '260000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-14','dataUpdate' => '2016-12-14'),
array('id' => '5828','ci' => '2784','tipo' => '11','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Condomínio Marítimo','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Ótimo terreno em condomínio fechado com total infraestrutura.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilsandro','id_cliente' => '5757','valor' => '200000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Pega carro, parcela, a vistafaz 150,000. Lote 16, Quadra 12','chaves' => '','data' => '2016-12-14','dataUpdate' => '2016-12-14'),
array('id' => '5831','ci' => '2785','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'João Pessoa. Ed. Salzburg','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento novo de aproximadamente 135m²  com 3 dormitórios sendo 1 suíte, sala de estar e jantar, banheiro social, cozinha, churrasqueira, lavanderia, box 2 carros.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5759','valor' => '648000.00','image' => '1481893679_Salzburg.jpg|1481893633_DSCF9187.jpg|1481893636_DSCF9191.jpg|1481893637_DSCF9197.jpg|1481893639_DSCF9198.jpg|1481893641_DSCF9199.jpg|1481893644_DSCF9201.jpg|1481893647_DSCF9203.jpg|1481893649_DSCF9204.jpg|1481893652_DSCF9208.jpg','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => 'Apto 1302 e 1402. 600Livre.','chaves' => 'Arsul alexandre','data' => '2016-12-16','dataUpdate' => '2016-12-16'),
array('id' => '5833','ci' => '2786','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Rua Rolante 246','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Residência com 3 dormitórios sendo 1 suíte, sala em 2 ambientes, cozinha, banheiro social, área de serviço, garagem. Anexo com churrasqueira, piscina com deck de madeira, posição solar frente leste (nascente), distante apenas 200m da barra de Imbé e 250m do mar, totalmente mobiliada.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5761','valor' => '500000.00','image' => '1481914025_WhatsApp_Image_20161216_at_163134jpeg.jpg|1481913988_WhatsApp_Image_20161216_at_163129_1jpeg.jpg|1481913989_WhatsApp_Image_20161216_at_163129jpeg.jpg|1481913990_WhatsApp_Image_20161216_at_163131jpeg.jpg|1481913991_WhatsApp_Image_20161216_at_163132jpeg.jpg|1481913992_WhatsApp_Image_20161216_at_163137_1jpeg.jpg|1481913992_WhatsApp_Image_20161216_at_163158_1jpeg.jpg|1481913993_WhatsApp_Image_20161216_at_163158jpeg.jpg|1481913993_WhatsApp_Image_20161216_at_163159jpeg.jpg|1481913994_WhatsApp_Image_20161216_at_163200jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Estuda tudo, doido p cambio. Parceria com Medeiros.','chaves' => 'Medeiros','data' => '2016-12-16','dataUpdate' => '2016-12-16'),
array('id' => '5834','ci' => '2787','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua Geraldo Santana 1243, esq. 1º Março','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Sobrado excelente com 4 dormitórios sendo 2 suítes, sacada, cozinha planejada, área de serviço, rebaixamento em gesso, garagem para 2 carros, sala de estar/jantar, lareira, salão de festas, piscina, 2 banheiros, amplo pátio, 2 terrenos.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5387','valor' => '980000.00','image' => '1482177693_WhatsApp_Image_20161219_at_170646_1jpeg.jpg|1482177614_WhatsApp_Image_20161219_at_170618jpeg.jpg|1482177616_WhatsApp_Image_20161219_at_170619jpeg.jpg|1482177618_WhatsApp_Image_20161219_at_170620jpeg.jpg|1482177620_WhatsApp_Image_20161219_at_170621jpeg.jpg|1482177623_WhatsApp_Image_20161219_at_170622_1jpeg.jpg|1482177625_WhatsApp_Image_20161219_at_170622_2jpeg.jpg|1482177627_WhatsApp_Image_20161219_at_170622jpeg.jpg|1482177629_WhatsApp_Image_20161219_at_170645jpeg.jpg|1482177631_WhatsApp_Image_20161219_at_170647jpeg.jpg','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => 'Estuda, intenção capitalizar.','chaves' => '','data' => '2016-12-19','dataUpdate' => '2016-12-19'),
array('id' => '5835','ci' => '2788','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua Belo Horizinte esq. com São Paulo','bairro' => 'Nova Tramandaí','estado' => 'RS','distancia' => '','descricao' => 'Linda casa com 2 quartos, sala, cozinha, banheiro, água quente, lareira,  piso de porcelanato, porta pivotante, janelas em pvc, construção de excelente qualidade.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '3011','valor' => '195000.00','image' => '1482178162_WhatsApp_Image_20161219_at_094956_1jpeg.jpg|1482178163_WhatsApp_Image_20161219_at_094957_1jpeg.jpg|1482178164_WhatsApp_Image_20161219_at_094957_2jpeg.jpg|1482178164_WhatsApp_Image_20161219_at_094957jpeg.jpg|1482178165_WhatsApp_Image_20161219_at_094958_2jpeg.jpg|1482178165_WhatsApp_Image_20161219_at_094958jpeg.jpg|1482178166_WhatsApp_Image_20161219_at_094959jpeg.jpg|1482178166_WhatsApp_Image_20161219_at_095024_1jpeg.jpg|1482178168_WhatsApp_Image_20161219_at_095024jpeg.jpg|1482178169_WhatsApp_Image_20161219_at_095025jpeg.jpg','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-19','dataUpdate' => '2016-12-19'),
array('id' => '5836','ci' => '2789','tipo' => '13','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Av. Beira Mar','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Duplex de excelente qualidade com 2 dormitórios, cozinha ampla, garagem com churrasqueira e linda vista para o mar. Mobiliado.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Victor','id_cliente' => '5732','valor' => '270000.00','image' => '1482178726_mallorca.jpg|1482178684_WhatsApp_Image_20161219_at_181508_1jpeg.jpg|1482178685_WhatsApp_Image_20161219_at_181508jpeg.jpg|1482178687_WhatsApp_Image_20161219_at_181510jpeg.jpg|1482178687_WhatsApp_Image_20161219_at_181511jpeg.jpg|1482178688_WhatsApp_Image_20161219_at_181512jpeg.jpg|1482178689_WhatsApp_Image_20161219_at_181513jpeg.jpg|1482178689_WhatsApp_Image_20161219_at_181515_1jpeg.jpg|1482178690_WhatsApp_Image_20161219_at_181515jpeg.jpg|1482178690_WhatsApp_Image_20161219_at_181519jpeg.jpg','destaque' => '','tempo' => '','ativo' => 'ativo','lancamento' => '','etiqueta' => '24x direto, a vista 210 Livre','chaves' => 'Zelador 996346232','data' => '2016-12-19','dataUpdate' => '2016-12-19'),
array('id' => '5837','ci' => '2790','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua Marques do Herval, 2495','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa com 4 dormitórios, 2 suítes, 1 banheiro social, sala de estar e jantar ampla, cozinha, varanda, área de serviço, garagem fechada churrasqueira, pátio fechado. Área construída 170m². Área do terreno 450m². ','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '5762','valor' => '350000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'Everton','data' => '2016-12-20','dataUpdate' => '2016-12-20'),
array('id' => '5838','ci' => '2791','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua Amapá, 1262','bairro' => 'Nova Tramandaí','estado' => 'RS','distancia' => '','descricao' => 'Casa com 3 dormitórios, 1 suíte, sala de estar e jantar, cozinha, varanda, área de serviço, garagem fechada, churrasqueira, pátio fechado. Área total construída 115m² e área do terreno 300m².','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '4745','valor' => '270000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'JV Imóveis','data' => '2016-12-20','dataUpdate' => '2016-12-20'),
array('id' => '5839','ci' => '2792','tipo' => '11','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua Casemiro Viecelli','bairro' => 'Zona Nova Sul','estado' => 'RS','distancia' => '','descricao' => 'terreno de 300m².','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '4745','valor' => '140000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Quadra 159 e Lotes 03,04','chaves' => '','data' => '2016-12-20','dataUpdate' => '2016-12-20'),
array('id' => '5840','ci' => '2793','tipo' => '11','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => '','bairro' => 'Nova Tramandaí','estado' => 'RS','distancia' => '','descricao' => 'Terreno de 300m²','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '4745','valor' => '60000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Lote 44 e Quadra D-24','chaves' => '','data' => '2016-12-20','dataUpdate' => '2016-12-20'),
array('id' => '5841','ci' => '2794','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => '24 de setembro esq. Flores da Cunha -Ed. Morano Calabro','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Morano Cálabro Residencial, em Tramandaí, de esquina para 2 das principais avenidas de Tramandaí. São 2 dormitórios, sendo 1 suíte, banheiro social, living amplo estar e jantar, sacada integrada, cozinha, churrasqueira, área de serviço.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '4745','valor' => '350000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'ESR Imóveis','data' => '2016-12-20','dataUpdate' => '2016-12-20'),
array('id' => '5857','ci' => '2795','tipo' => '11','modalidade' => 'Venda','local' => 'Atlântida','endereco' => 'Laguna','bairro' => 'Atlantida Sul','estado' => 'RS','distancia' => '','descricao' => 'Terreno urbano com 12x25m.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '3930','valor' => '100000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Lote 09 Quadra 34','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5856','ci' => '2796','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => '24 setembro 1012','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Chale 4 dormitórios, 2 banheiros, sala de estar, lavanderia, cozinha, sótão. Terreno 15x30. ','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5771','valor' => '865000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'Cindapa','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5853','ci' => '2797','tipo' => '11','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Av. Osorio 832','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Lote em avenida principal, medindo 15x30 posição solar frente mar.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5768','valor' => '540000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'dividir 3 irmãos aceita carro e imóvel menos valor.','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5850','ci' => '2798','tipo' => '13','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Rua 21 Emboaba, nº248','bairro' => 'Emboaba','estado' => 'RS','distancia' => '','descricao' => 'Casa medindo 75m² com 2 dormitórios, 1 banheiro, sala, cozinha, área de serviço, pátio, garagem para dois carros. ','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Bruno','id_cliente' => '4706','valor' => '138000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-28','dataUpdate' => '2016-12-28'),
array('id' => '5849','ci' => '2800','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Canguçu, 1611, próximo Av. Caxias','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa alvenaria 3 dormitórios sendo 1 suíte, sala estar, cozinha, sala de jantar, garagem com churrasqueira, 3 banheiros, piscina e móveis sob medida.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5766','valor' => '325000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '300 Livre.','chaves' => '','data' => '2016-12-28','dataUpdate' => '2016-12-28'),
array('id' => '5848','ci' => '2801','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Canguçu, 499','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Sobrado 232m², com piscina, 5 dormitórios destes 2 suítes, sendo 1 com hidro. Sala de estar com lareira e rebaixamento em gesso, cozinha, 2 banheiros social. Quiosque com banheiro e churrasqueira.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5765','valor' => '650000.00','image' => '1483120745_Slide1.jpg|1483120746_Slide2.jpg|1483120747_Slide6.jpg|1483120748_Slide7.jpg|1483120749_Slide8.jpg|1483120754_Slide9.jpg|1483120756_Slide11.jpg|1483120760_Slide14.jpg|1483120763_Slide24.jpg|1483120766_Slide27.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Troca apartamento com 3 dormitórios, av. da igreja ou Ap. em POA bairro Bom fim.','chaves' => '','data' => '2016-12-28','dataUpdate' => '2016-12-28'),
array('id' => '5847','ci' => '2802','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Av. Fernandes Bastos- Ed. Gaivota ','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento de 2 dormitórios, sala, cozinha, banheiro, frente vista para lagoa.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Diogo','id_cliente' => '5764','valor' => '240000.00','image' => '1482930247_WhatsApp_Image_20161226_at_170627jpeg.jpg|1482930229_WhatsApp_Image_20161226_at_170620jpeg.jpg|1482930230_WhatsApp_Image_20161226_at_170622jpeg.jpg|1482930231_WhatsApp_Image_20161226_at_170623jpeg.jpg|1482930232_WhatsApp_Image_20161226_at_170627_1jpeg.jpg|1482930232_WhatsApp_Image_20161226_at_170628_1jpeg.jpg|1482930233_WhatsApp_Image_20161226_at_170629jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '240 condomínio.','chaves' => '','data' => '2016-12-28','dataUpdate' => '2016-12-28'),
array('id' => '5846','ci' => '2803','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Canguçu, 2034','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa em alvenaria, 3 dormitórios sendo 1 suíte, sala com 2 ambientes, cozinha com espaço gourmet, área de serviço, com piscina e abrigo para 6 carros. Mobiliada.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilsandro','id_cliente' => '2522','valor' => '500000.00','image' => '1482928413_WhatsApp_Image_20161226_at_183905jpeg.jpg|1482928415_WhatsApp_Image_20161226_at_183906_1jpeg.jpg|1482928416_WhatsApp_Image_20161226_at_183906jpeg.jpg|1482928417_WhatsApp_Image_20161226_at_183907_1jpeg.jpg|1482928417_WhatsApp_Image_20161226_at_183907jpeg.jpg|1482928418_WhatsApp_Image_20161226_at_183908_1jpeg.jpg|1482928419_WhatsApp_Image_20161226_at_183908jpeg.jpg|1482928419_WhatsApp_Image_20161226_at_183909jpeg.jpg|1482928420_WhatsApp_Image_20161226_at_183910jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'estuda tudo.','chaves' => '','data' => '2016-12-28','dataUpdate' => '2016-12-28'),
array('id' => '5858','ci' => '2804','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => '24 de setembro 455- Ed. Sonrento','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento 03 dormitorios, sala de estar, cozinha, churrasqueira, lavanderia e box.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Débora','id_cliente' => '5772','valor' => '400000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Esta alugado.','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5859','ci' => '2805','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Manoela Cezario','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Ótima casa com 3 suítes, 2 salas amplas com lareira, cozinha estilo americana, escritório, abrigo para 3 carros, piscina. Toda mobiliada.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilsandro','id_cliente' => '5773','valor' => '1100000.00','image' => '1483114585_WhatsApp_Image_20161229_at_162256_1jpeg.jpg|1483114089_WhatsApp_Image_20161228_at_123150_1jpeg.jpg|1483114090_WhatsApp_Image_20161228_at_123150jpeg.jpg|1483114091_WhatsApp_Image_20161228_at_123159jpeg.jpg|1483114092_WhatsApp_Image_20161228_at_123200jpeg.jpg|1483114093_WhatsApp_Image_20161228_at_123340_1jpeg.jpg|1483114093_WhatsApp_Image_20161228_at_123340jpeg.jpg|1483114094_WhatsApp_Image_20161228_at_123343jpeg.jpg|1483114094_WhatsApp_Image_20161228_at_125006jpeg.jpg|1483114095_WhatsApp_Image_20161228_at_125007jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'aceita imóvel ou parcelamento.','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5860','ci' => '2806','tipo' => '7','modalidade' => 'Venda','local' => 'Jaguaruna','endereco' => 'Estrada Geral da Cigana s/n','bairro' => 'Farol de Santa Marta','estado' => 'RS','distancia' => '','descricao' => 'Casa excelente com 2 dormitórios sendo 1 suíte, sala, cozinha americana, banheiro social, aberturas e escadas em madeira nobre, terreno 12x30, toda murada e com pátio gramado, varanda com pilares de madeira tratada, excelente local p/ descanso, próximo ao mar, fácil acesso a cidade.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5774','valor' => '250000.00','image' => '1483120431_WhatsApp_Image_20161229_at_150222_1jpeg.jpg|1483120411_WhatsApp_Image_20161229_at_150203jpeg.jpg|1483120411_WhatsApp_Image_20161229_at_150209jpeg.jpg|1483120412_WhatsApp_Image_20161229_at_150212jpeg.jpg|1483120412_WhatsApp_Image_20161229_at_150213jpeg.jpg|1483120413_WhatsApp_Image_20161229_at_150217jpeg.jpg|1483120413_WhatsApp_Image_20161229_at_150219_1jpeg.jpg|1483120414_WhatsApp_Image_20161229_at_150220jpeg.jpg|1483120414_WhatsApp_Image_20161229_at_150222jpeg.jpg|1483120415_WhatsApp_Image_20161229_at_150226jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'estuda proposta','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5861','ci' => '2807','tipo' => '13','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Rua Bagé 2066','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Duplex com 3 dormitórios sendo 1 suíte, sala e cozinha estilo americana, churrasqueira, garagem fechada de vidro, escada em granito, cabeamento para TV a cabo e internet, espaço para 3 carros, esquadrias em alumínio com persiana em nylon, piso porcelanato em todos os banheiros, soleiras e pingadeiras em granito, previsão para alarme, previsão para água quente e ar condicionado.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Gilmar','id_cliente' => '5775','valor' => '430000.00','image' => '1483038160_WhatsApp_Image_20161229_at_144529jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Estuda proposta e parcela.','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5862','ci' => '2808','tipo' => '9','modalidade' => 'Venda','local' => 'Porto Alegre','endereco' => 'Frente Urgs','bairro' => '','estado' => 'RS','distancia' => '','descricao' => 'Ótimo apartamento em frente a URGS constituído por 3 dormitórios, 1 sala, cozinha, banheiro e área de serviço.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Diogo','id_cliente' => '5776','valor' => '324000.00','image' => '1483038744_inT456_11112358528c782194b.jpg|1483038724_inT456_11112358528bd2def21.jpg|1483038725_inT456_11112358528bd5e7305.jpg|1483038725_inT456_11112358528bd664f85.jpg|1483038726_inT456_11112358528bd48584e.jpg|1483038726_inT456_11112358528bd51646f.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5863','ci' => '2809','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Av. São Miguel esquina São Luiz 1355 ','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Residencia de alvenaria com 4 dormitórios, 1 suíte, 2 banheiros, área de serviço, garagem fechada e mobiliada.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Diogo','id_cliente' => '5777','valor' => '240000.00','image' => '','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => 'Cindapa','data' => '2016-12-29','dataUpdate' => '2016-12-29'),
array('id' => '5864','ci' => '2810','tipo' => '7','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Av. Paraguassu -Condomínio Por do Sol','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa com 290m², composta por 3 suítes sendo 1 master, na parte superior, 1 dormitório no térreo, sala estar 2 ambientes com lareira, espaço gourmet e piscina. Condomínio com marina e salão de festas, garagem para lancha.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Victor','id_cliente' => '5778','valor' => '1300000.00','image' => '1483535320_WhatsApp_Image_20170104_at_110007jpeg.jpg|1483535240_WhatsApp_Image_20170104_at_110012jpeg.jpg|1483535228_WhatsApp_Image_20170104_at_105935jpeg.jpg|1483535229_WhatsApp_Image_20170104_at_105939jpeg.jpg|1483535194_WhatsApp_Image_20170104_at_105940jpeg.jpg|1483535194_WhatsApp_Image_20170104_at_105953jpeg.jpg|1483535194_WhatsApp_Image_20170104_at_110101jpeg.jpg|1483535195_WhatsApp_Image_20170104_at_110105_1jpeg.jpg|1483535195_WhatsApp_Image_20170104_at_110105jpeg.jpg|1483535196_WhatsApp_Image_20170104_at_110108jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => '','chaves' => '','data' => '2017-01-02','dataUpdate' => '2017-01-02'),
array('id' => '5865','ci' => '2811','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Madalena Conceição,707','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Casa de alvenaria, com 4 dormitórios, sala, cozinha, área de serviço, 2 banheiros, sacada, garagem com churrasqueira, portão eletrônico, poço artesiano 120m de profundidade. ','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Zeca','id_cliente' => '2229','valor' => '270000.00','image' => '1483534113_WhatsApp_Image_20170103_at_142613jpeg.jpg|1483534068_WhatsApp_Image_20170103_at_142551jpeg.jpg|1483534069_WhatsApp_Image_20170103_at_142552jpeg.jpg|1483534070_WhatsApp_Image_20170103_at_142555jpeg.jpg|1483534071_WhatsApp_Image_20170103_at_142559jpeg.jpg|1483534071_WhatsApp_Image_20170103_at_142607jpeg.jpg|1483534072_WhatsApp_Image_20170103_at_142610jpeg.jpg|1483534073_WhatsApp_Image_20170103_at_142615jpeg.jpg|1483534073_WhatsApp_Image_20170103_at_142617jpeg.jpg|1483534074_WhatsApp_Image_20170103_at_142620jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'doc. averbado junto a PMT. Estuda proposta.','chaves' => '','data' => '2017-01-04','dataUpdate' => '2017-01-04'),
array('id' => '5866','ci' => '2812','tipo' => '13','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Sobradinho 1970','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Excelente duplex 77m², 2 dormitórios, banheiro, sacada, mezanino, sala de estar e jantar integradas, cozinha, pátio interno com lavanderia, banheiro e churrasqueira. Sem mobília.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Zeca','id_cliente' => '5779','valor' => '184000.00','image' => '1483537291_WhatsApp_Image_20170104_at_095208jpeg.jpg|1483537292_WhatsApp_Image_20170104_at_095209jpeg.jpg|1483537293_WhatsApp_Image_20170104_at_095412jpeg.jpg|1483537294_WhatsApp_Image_20170104_at_095422jpeg.jpg|1483537294_WhatsApp_Image_20170104_at_095428jpeg.jpg|1483537295_WhatsApp_Image_20170104_at_095429jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Doc ok, Ligar antes de mostrar.','chaves' => 'Local','data' => '2017-01-04','dataUpdate' => '2017-01-04'),
array('id' => '5867','ci' => '2813','tipo' => '13','modalidade' => 'Venda','local' => 'Imbé','endereco' => 'Av. Carazinho- Condomínio Las Olas ','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Residência com 2 pavimentos, 2 dormitórios, 1 banheiro social, sala, cozinha, churrasqueira, lavanderia, pátio, estacionamento para 1 carro. Condomínio com segurança 24 horas, com piscina com borda infinita vista para o mar, salão de festas com espaço gourmet, campo de futebol, quadra de vôlei, fitness center, espaço kids, murado com cerca elétrica e câmeras de segurança.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Zeca','id_cliente' => '5780','valor' => '314000.00','image' => '1483547848_unspecified_18.jpg|1483547828_unspecified_8.jpg|1483547828_unspecified_12.jpg|1483547829_unspecified_24.jpg|1483547830_unspecified_26.jpg|1483547830_unspecified_29.jpg|1483547831_unspecified_30.jpg|1483547832_unspecified_32.jpg|1483547833_unspecified_34.jpg|1483547833_unspecified_35.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Condomínio R$ 400. R$ 90,000 Livre ou R$ 290,000 quitado. Estuda proposta. Motivo: divorcio.','chaves' => 'Ligar Autoriza','data' => '2017-01-04','dataUpdate' => '2017-01-04'),
array('id' => '5868','ci' => '2814','tipo' => '9','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Emancipação 1130, Ed. Toronto','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Apartamento com 81m² privativo, 2 dormitórios, sendo 1 suíte, semi mobiliado, sala de estar e jantar integrados, cozinha americana, lavanderia separada, banheiro social. Orientação solar Leste. Vaga para 2 carros.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Victor','id_cliente' => '5781','valor' => '420000.00','image' => '1483548110_Toronto_CI_885.jpg|1483548043_WhatsApp_Image_20170104_at_104504jpeg.jpg|1483548044_WhatsApp_Image_20170104_at_104507_1jpeg.jpg|1483548045_WhatsApp_Image_20170104_at_104507_2jpeg.jpg|1483548045_WhatsApp_Image_20170104_at_104507jpeg.jpg|1483548046_WhatsApp_Image_20170104_at_104509_1jpeg.jpg|1483548046_WhatsApp_Image_20170104_at_104509jpeg.jpg|1483548047_WhatsApp_Image_20170104_at_104517_1jpeg.jpg|1483548047_WhatsApp_Image_20170104_at_104517jpeg.jpg|1483548048_WhatsApp_Image_20170104_at_104518jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Ap. 905','chaves' => '','data' => '2017-01-04','dataUpdate' => '2017-01-04'),
array('id' => '5869','ci' => '2815','tipo' => '7','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Emancipação 2111','bairro' => 'Centro','estado' => 'RS','distancia' => '','descricao' => 'Sobrado com 2 terrenos medindo 340m² de área construída, 3 dormitórios, sendo 1 suíte, sala com 2 ambientes, lareira, anexo com garagem para 2 carros e estacionamento, telha de concreto, sacada, dependência de empregada com banheiro.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Zeca','id_cliente' => '5782','valor' => '1800000.00','image' => '1483548704_WhatsApp_Image_20170104_at_140341jpeg.jpg|1483548682_WhatsApp_Image_20170104_at_140249jpeg.jpg|1483548683_WhatsApp_Image_20170104_at_140253jpeg.jpg|1483548683_WhatsApp_Image_20170104_at_140257jpeg.jpg|1483548684_WhatsApp_Image_20170104_at_140308jpeg.jpg|1483548685_WhatsApp_Image_20170104_at_140343jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Doc ok, estuda sobre mobília, estuda propostas com possibilidades de entrar outros imóveis no negócio.','chaves' => 'Local','data' => '2017-01-04','dataUpdate' => '2017-01-04'),
array('id' => '5870','ci' => '2816','tipo' => '12','modalidade' => 'Venda','local' => 'Tramandaí','endereco' => 'Av. Fernandes Bastos 1668','bairro' => 'Tiroleza','estado' => 'RS','distancia' => '','descricao' => 'Prédio comercial e residencial, composto de 2 terrenos de 24x21.','itens' => 'a:5:{s:11:"Dormitórios";s:0:"";s:9:"Banheiros";s:0:"";s:7:"Garagem";s:0:"";s:7:"Piscina";s:0:"";s:6:"Suítes";s:0:"";}','caracteristicas' => '','corretor' => 'Zeca','id_cliente' => '5783','valor' => '2000000.00','image' => '1483549075_ci_2816jpeg.jpg','destaque' => '','tempo' => '','ativo' => '','lancamento' => '','etiqueta' => 'Prédio não esta regularizado junto a PMT. Estuda Propostas. Q. 20 L. 5','chaves' => 'Local','data' => '2017-01-04','dataUpdate' => '2017-01-04')
);


    public function formatar_responsaveis()
    {
        $telefones = $this->db->query("SELECT telefone_1, COUNT(*) c FROM tb_imovel_responsavel WHERE telefone_1 != '' GROUP BY telefone_1 HAVING c > 1");

        foreach ($telefones->result() as $row)
        {
            $responsaveis = $this->db->query("SELECT * from `tb_imovel_responsavel` WHERE telefone_1 = '$row->telefone_1'")->result();

            //$id_responsavel = $responsaveis[0]->id;

            $data = array(
                'id_responsavel' => $responsaveis[0]->id
            );

            //die;
            //echo $id_responsavel . '<br>';

            $ids = array();
            foreach($responsaveis as $responsavel)
            {
                if($responsavel->id != $responsaveis[0]->id)
                    $ids[] = $responsavel->id;
            }

            //ATUALIZA
            $this->db->set('id_responsavel', $responsaveis[0]->id);
            $this->db->where_in('id_responsavel', $ids);
            $this->db->update('tb_imovel_detalhes', $data);

            $this->db->where_in('id', $ids);
            $this->db->delete('tb_imovel_responsavel');
        }
    }

}