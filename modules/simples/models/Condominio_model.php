<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Empreendimento_model.php');

class Condominio_Model extends Empreendimento_Model
{
    /*protected $table = 'view_condominio';
    protected $tb_foto = 'tb_condominio_foto';
    protected $tb_foto_foreign_key = 'id_condominio';
    protected $tb_complemento = 'tb_complemento';
    */

    protected $table = 'view_condominio';
    protected $tb_foto = 'tb_condominio_foto';
    public $tb_foto_foreign_key = 'id_condominio';
    public $tb_video = 'tb_condominio_video';
    public $tb_video_foreign_key = 'id_condominio';
    protected $tb_empreendimento_complemento = 'tb_condominio_complemento';
    protected $tb_complemento_foreign_key = 'id_condominio';
    //protected $tb_condominio_complemento = 'tb_condominio_complemento';
    protected $tb_unidades = 'view_condominio_unidade';
    protected $tb_unidades_foreign_key = 'id_condominio';

    public function todos_horizontais_informacoes_basicas($foto_principal = true)
    {
        $this->load->library('simples/CondominioTipos');

        if($foto_principal)
            $this->select_foto_principal('id, nome, fechado, cidade, condominio_tipo');

        return $this->db
                ->where('condominio_tipo', CondominioTipos::Horizontal)
                    ->get($this->table)
                        ->result();
    }

    public function unidades($id)
    {
        return $this->db
            ->where($this->tb_unidades_foreign_key, $id)
                ->get($this->tb_unidades)
                    ->result();
    }

    public function todos_fechados($foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db
                    ->where('fechado', 1)
                        ->get($this->table)
                            ->result();
    }

    public function todos_lancamentos($foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db
                    ->where('lancamento', 1)
                        ->get($this->table)
                            ->result();
    }
}