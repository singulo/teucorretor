<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

/**
 * @property CI_DB_mysql_driver $db
 */
class Clientes_Model extends Base_Model {

	public $id;
	public $nome;
	public $email;
	public $email_alternativo;
	public $telefone_1;
	public $telefone_2;
	public $telefone_3;
	public $cidade;
	public $uf;
	public $bloqueado;
	public $excluido;
	public $id_corretor;
	public $aniversario;
	//public $status;
	public $obs;
	public $imovel_litoral;
	public $cidade_imovel;
	public $ano_aquisicao;
	public $valor_imovel;
	public $imovel_negocio;
	public $previsao_investimento;
	public $interesse_cidade;
	public $interesse_dormitorios;
	public $interesse_tipo;
	public $interesse_valor;
	public $interesse_entrada;
	public $interesse_saldo;
	public $cadastrado_em;
	public $ultimo_acesso;
	public $atualizado_em;
	public $atualizado_por;

	protected $table = 'tb_clientes';

	public function pelo_email($email)
	{
		return $this->db
			->where('email', $email)
			->get($this->table)->first_row();
	}

	public function editar(Clientes_Model $cliente, $registrar_copia = true)
	{
		//COPIA REGISTRO ANTES DA EDIÇÃO E SALVA NA TABELA DE AUDIÇÃO
		$cliente_copia = $this->obter($cliente->id);

		unset($cliente->table);
		unset($cliente->db);
		$cliente = array_filter((array)$cliente, function($val) { return ($val || is_numeric($val));});

		$this->db->where('id', $cliente['id']);
		$this->db->update($this->table, $cliente);
		$linhas_afetadas = $this->db->affected_rows();

		if($linhas_afetadas > 0 && $registrar_copia)
			$this->db->insert('tb_clientes_historico', $cliente_copia);

		return $linhas_afetadas;
	}

	/*public function consulta_cliente_email($email)
	{
		$this->db->where('email', $email);
		return $this->db->get($this->table)->first_row();
	}

	public function consulta_cliente_id($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->table)->first_row();
	}



	public function filtro_total(array $filtro)
	{
		$this->monta_filtro($filtro);
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function filtrar(array $filtro, $limite, $pagina)
	{
		$this->monta_filtro($filtro);

		$this->db->order_by('ultimo_acesso', 'DESC');
		$this->db->limit($limite, ($pagina * $limite));
		return $this->db->get($this->table)->result();
	}

	private function monta_filtro(array $filtro)
	{
		$this->db->where('excluido', false); //NÃO EXIBIR OS EXCLUIDOS

		foreach($filtro as $campo => $valor){

			if($this->endsWith($campo, '_inicio')){
				$this->db->where(str_replace('_inicio', '', $campo	) . ' >=', $valor);
			}
			else if($this->endsWith($campo, '_fim')){
				$this->db->where(str_replace('_fim', '', $campo	) . ' <=', $valor);
			}
			else if($campo == 'id_corretor')
				$this->db->where($campo, $valor);
			else
				$this->db->like($campo, $valor);
		}
	}

	public function pelo_perfil(array $cidades, array $tipos, $interesse_valor_min = NULL, $interesse_valor_max = NULL, $id_corretor = NULL)
	{
		if(!is_null($id_corretor))
			$this->db->where('id_corretor', $id_corretor);

		if(!is_null($interesse_valor_min) && !empty($interesse_valor_min))
			$this->db->where('interesse_valor >=', $interesse_valor_min);

		if(!is_null($interesse_valor_max) && !empty($interesse_valor_max))
			$this->db->where('interesse_valor <=', $interesse_valor_max);

		$this->db->group_start();
			foreach($cidades as $cidade)
				$this->db->or_like('interesse_cidade', $cidade);

			foreach($tipos as $tipo)
				$this->db->or_like('interesse_tipo', $tipo);
		$this->db->group_end();

		return $this->db->select('email')
						->get($this->table)
						->result();
	}

	public function com_os_email(array $emails)
	{
		return $this->db->where_in('email', $emails)
						->get($this->table)
						->result();
	}

	public function bloquear($id, $por, $bloqueado)
	{
		$cliente = new Clientes_Model();
		$cliente->id = $id;
		$cliente->bloqueado = (int)$bloqueado;
		$cliente->atualizado_em = date("Y-m-d H:i:s");
		$cliente->atualizado_por = $por;
		return $this->editar($cliente);
	}

	public function excluir($id, $por, $excluido)
	{
		$cliente = new Clientes_Model();
		$cliente->id = $id;
		$cliente->excluido = (int)$excluido;
		$cliente->atualizado_em = date("Y-m-d H:i:s");
		$cliente->atualizado_por = $por;
		return $this->editar($cliente);
	}

	public function total_por_corretor($id_corretor, $excluidos = false)
	{
		if(!$excluidos)
			$this->db->where('excluido', false); //NÃO EXIBIR OS EXCLUIDOS

		return $this->db->where('id_corretor', $id_corretor)->count_all_results($this->table);
	}

	public function ultimos_cadastrados($limit, $id_corretor = null, $excluidos = false)
	{
		if(!$excluidos)
			$this->db->where('excluido', false); //NÃO EXIBIR OS EXCLUIDOS

		if(!is_null($id_corretor))
			$this->db->where('id_corretor', $id_corretor);

		return $this->db->order_by('cadastrado_em DESC')
			->limit($limit)
			->get($this->table)
			->result();
	}

	public function trocar_corretor($de, $para)
	{
		$this->db
			->where('id_corretor', $de)
			->update($this->table, array('id_corretor' => $para));

		return	$this->db->affected_rows();
	}

	public function total_cadastrados_em($data, $excluidos = false)
	{
		if(!$excluidos)
			$this->db->where('excluido', false); //NÃO EXIBIR OS EXCLUIDOS

		return $this->db->where('DATE(cadastrado_em)', $data)->count_all_results($this->table);
	}

	public function total_acessos_em($data, $excluidos = false)
	{
		if(!$excluidos)
			$this->db->where('excluido', false); //NÃO EXIBIR OS EXCLUIDOS

		return $this->db->where('DATE(ultimo_acesso)', $data)->count_all_results($this->table);
	}*/
}