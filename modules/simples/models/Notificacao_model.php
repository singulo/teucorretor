<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Notificacao_Model extends Base_Model {

	public $id;
	public $tipo;
	public $nome;
	public $email;
	public $telefone;
	public $criado_em;
	public $respondido_em;
	public $assunto;
	public $id_corretor;

	protected $table = 'view_notificacoes';
	protected $tb_notificacao_corretor = 'tb_corretor_notificacao';

	public function cliente_vinculado($id_corretor_enviando, $id_corretor_para, $id_cliente = NULL, $mensagem = 'Um cliente foi vinculado a você.')
	{
		$notificacao = new NotificacaoCorretorDomain();
		$notificacao->id_corretor_enviou    = $id_corretor_enviando;
		$notificacao->id_corretor_para      = $id_corretor_para;
		$notificacao->id_cliente            = $id_cliente;
		$notificacao->tipo                  = 'cliente_vinculado';
		$notificacao->mensagem              = $mensagem;
		$notificacao->enviado_em            = date('Y-m-d H:i:s');

		$this->db->insert($this->tb_notificacao_corretor, $notificacao);
		return $this->db->insert_id();
	}

	public function marcar_como_respondida($id, $tipo)
	{
		return $this->trocar_status_notificacao($id, $tipo, true);
	}

	private function trocar_status_notificacao($id, $tipo, $respondida)
	{
		$tb_nao_respondido = '';
		$tb_respondido = '';
		$coluna = '';

		switch($tipo)
		{
			case 'normal':
				$tb_nao_respondido = 'tb_contato';
				$tb_respondido = 'tb_contato_respondido';
				$coluna = 'respondido_em';
				break;
			case 'interesse_em_imovel':
				$tb_nao_respondido = 'tb_contato_imovel_interesse';
				$tb_respondido = 'tb_contato_imovel_interesse_respondido';
				$coluna = 'respondido_em';
				break;
			case 'cliente_vinculado':
				$tb_nao_respondido = 'tb_corretor_notificacao';
				$tb_respondido = 'tb_corretor_notificacao_respondido';
				$coluna = 'lida_em';
				break;
		}

		if($respondida)
		{
			$notificacao = $this->db->where('id', $id)->get($tb_nao_respondido)->first_row();
			if( ! is_null($notificacao) ) //EXCLUI DA NOTIFICAÇÃO DA TABELA DE NÃO RESPONDIDAS
			{
				$notificacao->$coluna = date("Y-m-d H:i:s");
				//UTILIZADO INSERIR PARA VARIOS PELO MOTIVO DE OBTER A QUANTIDADE DE LINHAS AFETADAS MAIOR QUE ZERO E NAO O ID POR FORÇAR A INSERCAO DO ID
				return (($this->inserir_varios(array($notificacao), $tb_respondido) > 0) && ($this->deletar($id, 'id', $tb_nao_respondido) > 0));
			}
			else
				return false;
		}
		else
		{
			$notificacao = $this->db->where('id', $id)->get($tb_respondido)->first_row();

			if( ! is_null($notificacao) ) //EXCLUI DA NOTIFICAÇÃO DA TABELA DE RESPONDIDAS
			{
				$notificacao->$coluna = NULL;
				return ($this->inserir($notificacao, $tb_nao_respondido) > 0 && $this->deletar($id, 'id', $tb_respondido) > 0);
			}
			else
				return false;
		}
	}
}

class NotificacaoCorretorDomain
{
	public $id;
	public $id_corretor_enviou;
	public $id_corretor_para;
	public $id_cliente;
	public $mensagem;
	public $tipo;
	public $enviado_em;
}