<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Empreendimento_Model extends Base_Model
{
    protected $table;
    protected $tb_foto;
    public $tb_foto_foreign_key;
    public $tb_video;
    public $tb_video_foreign_key;
    protected $tb_empreendimento_complemento;
    protected $tb_complemento_foreign_key;
    protected $tb_complemento = 'tb_complemento';

    public function destaques($limite = 10, $rand = true, $tipos = array())
    {
        if($rand)
            $this->db->order_by('RAND()');

        if(count($tipos) > 0)
            $this->db->where_in('id_tipo', $tipos);

        return $this->select_foto_principal()
                                ->where('destaque', 1)
                                    ->limit($limite)
                                        ->get($this->table)
                                            ->result();
    }

    public function pelo_codigo($id, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->obter($id);
    }

    protected function select_foto_principal($add_ao_select = '*')
    {
        return $this->db->select("$add_ao_select, (SELECT arquivo FROM $this->tb_foto WHERE $this->tb_foto.$this->tb_foto_foreign_key = $this->table.id ORDER BY $this->tb_foto.destaque DESC LIMIT 1) as foto");
    }

    public function pelos_codigos(array $ids, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db
            ->where_in('id', $ids)
            ->get($this->table)
            ->result();
    }

    public function complementos($id)
    {
        return $this->db
            ->from($this->tb_complemento)
            ->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_complemento = $this->tb_complemento.id AND $this->tb_empreendimento_complemento.$this->tb_complemento_foreign_key = $id")
            ->get()
            ->result();
    }

    public function total_fotos($id)
    {
        return $this->db
            ->where($this->tb_foto_foreign_key, $id)
            ->where('mostrar_site', 1)
            ->from($this->tb_foto)
            ->count_all_results();
    }

    public function fotos($id, $limite = null)
    {
        if(!is_null($limite))
            $this->db->limit($limite);

        return $this->db
            ->where($this->tb_foto_foreign_key, $id)
            ->where('mostrar_site', 1)
            ->order_by("destaque DESC")
            ->get($this->tb_foto)
            ->result();
    }

    public function videos($id)
    {
        return $this->db
            ->where($this->tb_video_foreign_key, $id)
            ->get($this->tb_video)
            ->result();
    }
}