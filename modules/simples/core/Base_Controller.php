<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(__DIR__ . '/../helpers/filial_helper.php');

/**
 * @property Clientes_Model $clientes_model
 * @property Imobiliaria_Model $imobiliaria_model
 * @property CI_Session $session
 */
class Base_Controller extends MX_Controller {

	public function __construct($redirecionar_se_db_nao_setado = true)
	{
		parent::__construct();

		if($this->session->has_userdata('admin') && //LOGADO
			$this->uri->segment(1) == 'admin' || $this->uri->segment(1) == 'novo' && $this->uri->segment(2) == 'admin') //E 	ESTA ACESSANDO O ADMIN E ESTEJA EM CONSTRUCAO O NOVO SITE
		{

			if(isset($this->session->userdata('admin')->filial))
			{
				$_SESSION['filial'] = $this->session->userdata('admin')->filial;
				return;
			}
			else
				redirect(base_url('admin'));
		}
		else if(isset($_SESSION['filial'])
			&& isset($_SESSION['filial_usuario'])
			&& $_SESSION['filial']['chave'] != $_SESSION['filial_usuario']['chave'])
		{
			$_SESSION['filial'] = $_SESSION['filial_usuario'];
		}

		if(!isset($_SESSION['filial']) && count($this->config->item('filiais')) == 1)
		{
			$filiais = $this->config->item('filiais');
			$x = array_keys($filiais);
			$_GET['filial'] = $x[0];
		}

		if(isset($_GET['filial']))
		{
			armazena_filial_em_sessao();
		}
		else if(!isset($_SESSION['filial']) && $redirecionar_se_db_nao_setado)
		{	//CASO A FILIAL NAO ESTIVER SELECIONADA E O BANCO TBM NAO O USUARIO SERA REDIRECIONADO PARA A SELEÇÃO DA FILAL
			redirect('/');
		}

		//Destruir a sessão caso o usuário troque de filial estando logado em outra
		if($this->session->has_userdata('usuario'))
		{
			$cliente = $this->session->userdata('usuario');

			$this->load->model('simples/clientes_model');
			if($this->clientes_model->pelo_email($cliente->email) == NULL)
			{
				$this->session->unset_userdata('usuario');
				$this->session->sess_destroy();
			}
		}
	}
}
