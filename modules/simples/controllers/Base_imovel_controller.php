<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(MODULESPATH . 'simples/core/Base_Controller.php');
//require_once(APPPATH . '../modules/simples/libraries/Watermark.php');
require_once MODULESPATH . 'simples/helpers/cliente_helper.php';


/**
 * @property Imovel_Model $imovel_model
 * @property Condominio_Model $condominio_model
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 * @property CI_Session $session
 */
class Base_Imovel_Controller extends Base_Controller
{
	protected $quantidade_fotos_principais = 1;
	protected $quantidade_sugestoes = 6;
	protected $pesquisa_order_by = 'id_tipo ASC, valor ASC';
	protected $pesquisa_identificador_imovel = 'id';
	protected $filtro_parametros = array(
		array('param' => 'cidade',          'default_value' => array()),
		array('param' => 'id_tipo',         'default_value' => array()),
		array('param' => 'id_condominio',   'default_value' => array()),
		array('param' => 'finalidade',      'default_value' => array()),
		array('param' => 'preco_min',       'default_value' => 0),
		array('param' => 'preco_max',       'default_value' => 3000000),
		array('param' => 'id',       		'default_value' => ''),
		array('param' => 'dormitorios'),
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('simples/imovel_model');
	}

	public function index()
	{
		$data = array();

		if(isset($_GET['id']))
		{
			$data['imovel'] = $this->imovel_model->pelo_codigo($_GET['id'], false);

			if(!is_null($data['imovel']))
			{
				if($this->session->has_userdata('usuario'))
				{
					//ARMAZEMANA VISUALIZAO DE IMOVEL SE CLIENTE LOGADO
					$this->load->model('simples/clientes_imoveis_log_model');

					if($this->clientes_imoveis_log_model->novo($this->session->userdata('usuario')->email, $data['imovel']->id) == 0)
						die('Ocorreu um erro ao registrar a visualização do imóvel.');

					$this->load->library('simples/EmpreendimentoImagemTipos');
					$data['imovel']->fotos['normais'] = array();
					$data['imovel']->fotos['plantas'] = array();

					$fotos = $this->imovel_model->fotos($_GET['id']);
					foreach($fotos as $foto)
					{
						if(EmpreendimentoImagemTipos::Normal == $foto->tipo)
							$data['imovel']->fotos['normais'][] = $foto;
						else if(EmpreendimentoImagemTipos::Planta == $foto->tipo)
							$data['imovel']->fotos['plantas'][] = $foto;
					}

					$data['imovel']->fotos_principais = array();
					for ($i = 0; $i < count($fotos); $i++)
					{
						$data['imovel']->fotos_principais[] = $fotos[$i];
					}

					$data['imovel']->videos = $this->imovel_model->videos($data['imovel']->id);
				}
				else
				{
					$data['total_fotos'] = $this->imovel_model->total_fotos($_GET['id']);
					$data['imovel']->fotos_principais = $this->imovel_model->fotos($_GET['id'], $this->quantidade_fotos_principais);
				}

				$data['imovel']->complementos = $this->imovel_model->complementos($_GET['id']);

				if($data['imovel']->id_condominio > 0)
				{
					$this->load->model('simples/condominio_model');
					$data['imovel']->condominio = $this->condominio_model->obter($data['imovel']->id_condominio);
				}

				$data['imoveis_similares'] = $this->imovel_model->similares($data['imovel'], $this->quantidade_sugestoes);
			}

		}

		return $data;
	}

	public function condominios()
	{
		$this->load->model('admin/imoveis_model');
		$data['condominios'] = $this->imoveis_model->condominios();

		return $data;
	}

	public function lancamentos()
	{
		$this->load->model('admin/imoveis_model');
		$data['lancamentos'] = $this->imoveis_model->lancamentos('f_cidade ASC, f_condominio ASC');

		return $data;
	}

	public function pesquisar()
	{
		$data['filtro'] = $this->monta_filtro();

		return $data;
	}

	protected function monta_filtro()
	{
		if(strlen($_SERVER['QUERY_STRING']) == 0)
			return NULL;

		$query  = explode('&', $_SERVER['QUERY_STRING']);
		$params = array();

		foreach( $query as $param )
		{
			list($name, $value) = explode('=', $param, 2);
			$params[urldecode($name)][] = urldecode($value);
		}

		foreach( $params as $key => $param)
		{
			if(is_array($param) && count($param) == 1)
				$params[$key] = $param[0];
		}

		$filtro = new stdClass();

		foreach($this->filtro_parametros as $parametro)
		{
			if(isset($params[$parametro['param']]))
				$filtro->$parametro['param'] = $params[$parametro['param']];
			else if(isset($parametro['default_value']))
				$filtro->$parametro['param'] = $parametro['default_value'];
		}

		return $filtro;
	}

	public function buscar()
	{
		$pagina = $this->input->post('pagina') != null ? $this->input->post('pagina') : 0;
		$limite = $this->input->post('limite') != null ? $this->input->post('limite') : 10;

		$filtro = $this->input->post('filtro');

		if($this->pesquisa_identificador_imovel != 'id')
		{
			$filtro[$this->pesquisa_identificador_imovel] = $filtro['id'];
			unset($filtro['id']);
		}

		if(is_null($filtro))
			$filtro = array();

		$data['imoveis'] = $this->imovel_model->pesquisar($filtro, $pagina, $limite, $this->pesquisa_order_by);
		if(count($data['imoveis']) == 0)
			$data['total'] = 0;
		else if($pagina == 0)
			$data['total'] = $this->imovel_model->pesquisar_total_resultados($filtro);

		return $data;
	}

	protected function transforma_valor_imovel($valor)
	{
		$valor = str_replace('.', '', $valor);
		$valor = str_replace(',', '.', $valor);

		return (double)filter_var($valor, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
	}
}