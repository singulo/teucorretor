<?php

    function obter_estados_brasil()
    {
        $estados['ac'] = 'Acre' ;
        $estados['al'] = 'Alagoas' ;
        $estados['am'] = 'Amazonas' ;
        $estados['ap'] = 'Amapá' ;
        $estados['ba'] = 'Bahia' ;
        $estados['ce'] = 'Ceará' ;
        $estados['df'] = 'Distrito Federal' ;
        $estados['es'] = 'Espírito Santo' ;
        $estados['go'] = 'Goiás' ;
        $estados['ma'] = 'Maranhão' ;
        $estados['mt'] = 'Mato Grosso' ;
        $estados['ms'] = 'Mato Grosso do Sul' ;
        $estados['mg'] = 'Minas Gerais' ;
        $estados['pa'] = 'Pará' ;
        $estados['pb'] = 'Paraíba' ;
        $estados['pr'] = 'Paraná' ;
        $estados['pe'] = 'Pernambuco' ;
        $estados['pi'] = 'Piauí' ;
        $estados['rj'] = 'Rio de Janeiro' ;
        $estados['rn'] = 'Rio Grande do Norte' ;
        $estados['rs'] = 'Rio Grande do Sul' ;
        $estados['ro'] = 'Rondônia' ;
        $estados['rr'] = 'Roraima' ;
        $estados['sc'] = 'Santa Catarina' ;
        $estados['se'] = 'Sergipe' ;
        $estados['sp'] = 'São Paulo' ;
        $estados['to'] = 'Tocantins' ;

        return $estados;
    }
