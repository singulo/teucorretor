<?php

function registra_cliente(array $dados, $id_corretor, $prefix = '')
{
    $CI = get_instance();

    require_once(__DIR__ . '/../libraries/RequestMapper.php');

    $mapper = array('cadastrado_em'  => array('default_value' => date("Y-m-d H:i:s")),
        'cidade'         => array('default_value' => ''),
        'bloqueado'      => array('default_value' => 0),
        'excluido'       => array('default_value' => 0),
        'atualizado_em'  => array('default_value' => date("Y-m-d H:i:s")),
        'atualizado_por' => array('default_value' => $dados['email']));

    $CI->load->model('simples/clientes_model');
    $cliente = RequestMapper::parseToObject($dados, $mapper, new Clientes_Model(), $prefix);

    $cliente->id_corretor = $id_corretor;

    $cliente->id = $CI->clientes_model->inserir($cliente);

    //ENVIAR EMAIl SE NOVO CADASTRO DE CLIENTE REGISTRADO COM SUCESSO
    if($cliente->id > 0){
        require_once(__DIR__ . '/email_helper.php');

        //EMAIL DE NOVO CADASTRO
        $corretor = obter_corretor_cliente($cliente);
        enviar_email($corretor->email,
            $cliente->nome . ' acaba de se CADASTRAR!',
            monta_corpo_email(array('nome'      => $corretor->nome,
                'assunto'	=> 'O cliente <strong>' . $cliente->nome .'</strong> acaba de se <strong>CADASTRAR</strong> pelo site!<br>Dados do cliente abaixo.',
                'cliente'	=> $cliente->nome,
                'email'		=> $cliente->email,
                'telefone'	=> $cliente->telefone_1,
                'cidade'	=> $cliente->cidade),
                'dados_do_cliente'));

        //EMAIL DE BOAS-VINDAS
        enviar_email($cliente->email,
            $cliente->nome . ' seja bem-vindo!',
            monta_corpo_email(array('nome'          => $cliente->nome,
                'assunto'	    => 'O seu cadastro já está ativo, agora você pode visualizar todos os imóveis do nosso site. Faça o seu login apenas informando o seu email.',
                'cliente_email'	=> $cliente->email),
                'cliente-boas-vindas'));

        $CI->load->model('simples/notificacao_model');
        $CI->notificacao_model->cliente_vinculado(0, $cliente->id_corretor, $cliente->id);

        return true;
    }

    return false;
}

function obter_corretor_cliente($cliente)
{
    $CI = get_instance();
    $CI->load->model('simples/corretores_model');

    if($cliente->id_corretor == NULL)//CASO O USUARIO NAO TENHA UM CORRETOR VINCULADO
        $cliente->id_corretor = $_SESSION['filial']['corretor_padrao_id'];

    return $CI->corretores_model->obter_informacoes_basicas($cliente->id_corretor);
}