<?php

function texto_para_plural_se_necessario($valor, $palavra, $plural = 's')
{
    if($valor > 1)
        $palavra .= $plural;

    return $palavra;
}