<?php


function armazena_filial_em_sessao()
{
    $CI = get_instance();

    $filiais = $CI->config->item('filiais');
    $CI->session->set_userdata('filial', $filiais[$_GET['filial']]);

    $CI->load->model('simples/imobiliaria_model');
    $imobs = $CI->imobiliaria_model->listar(0,1);

    if(isset($imobs[0]))
        $CI->session->set_userdata('filial', ((array)$imobs[0] + (array)$CI->session->userdata('filial')));

    $CI->load->model('simples/banner_model');
    $_SESSION['filial']['banners'] = $CI->banner_model->listar_todos();

    $CI->load->model('simples/imovel_model');
    $CI->load->model('simples/condominio_model');

    $cod_imoveis = array();
    foreach ($_SESSION['filial']['banners'] as $banner)
        $cod_imoveis[] = $banner->codigo_imovel;

    /** @var Imovel_Model $imovel_model */
    $imoveis = array();
    if(count($cod_imoveis) > 0)
        $imoveis = $CI->imovel_model->pelos_codigos($cod_imoveis);

    foreach($imoveis as $imovel)
    {
        foreach($_SESSION['filial']['banners'] as $banner)
        {
            if($banner->codigo_imovel == $imovel->id)
                $banner->imovel = $imovel;
        }
    }

    //CIDADES
    $_SESSION['filial']['cidades_imoveis'] = $CI->imovel_model->cidades_imoveis();
    //TIPOS DOS IMÓVEIS
    foreach($CI->imovel_model->tipos() as $tipo)
        $_SESSION['filial']['tipos_imoveis'][$tipo->id] = $tipo;

    //CONDOMÍNIOS
    $_SESSION['filial']['condominios_horizontais'] = $CI->condominio_model->todos_horizontais_informacoes_basicas();

    //ARMAZENADO EM
    $_SESSION['filial']['armazenada_em'] = date('Y-m-d H:i:s');

    //PARA ADICIONAR FUNCIONALIDADES ESPECIFICAS DE CADA SITE NECESSÁRIAS AO CARREGAMENTO
    if(file_exists(APPPATH."helpers/carregar_na_filial_helper.php"))
    {
        $CI->load->helper('carregar_na_filial_helper');
        if (function_exists('carregar_na_filial'))
            carregar_na_filial();
    }

    $_SESSION['filial_usuario'] = $_SESSION['filial'];
}