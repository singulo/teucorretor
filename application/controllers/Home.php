<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Imovel_Model $imovel_model
 * @property Condominio_Model $condominio_model
 */
class Home extends Base_Controller {

	public function index()
	{
		$this->load->model('simples/imovel_model');
		$this->load->model('simples/condominio_model');

		$data['condominio_destaque'] = $this->condominio_model->destaques(1);

		if(! is_null($data['condominio_destaque']) && isset( $data['condominio_destaque'][0]))
			$data['condominio_destaque'] = $data['condominio_destaque'][0];
		else
			$data['condominio_destaque'] = NULL;

		$data['imoveis_destaque'][] = array('titulo' => 'CASAS NO LITORAL', 'imoveis' => $this->imovel_model->destaques(10, true, array(7, 15)));
		$data['imoveis_destaque'][] = array('titulo' => 'APARTAMENTOS PRONTOS PARA MORAR', 'imoveis' => $this->imovel_model->destaques(10, true, array(6, 9)));
		$data['imoveis_destaque'][] = array('titulo' => 'DUPLEX E CASAS GEMINADAS', 'imoveis' => $this->imovel_model->destaques(10, true, array(13)));
		$data['imoveis_destaque'][] = array('titulo' => 'TERRENOS', 'imoveis' => $this->imovel_model->destaques(10, true, array(11)));

		$this->load->view('home', $data);
	}

	/*public function formatar_dados()
	{
		//$this->load->model('simples/condominio_model');

		//$this->imovel_model->formatar_dados();

		$this->load->model('simples/imovel_model');
		$this->imovel_model->adicionar_imoveis_novos();
	}
*/
	/*
	public function formatar_responsaveis()
	{
		$this->load->model('simples/imovel_model');
		$this->imovel_model->formatar_responsaveis();
	}*/
}
