<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require MODULESPATH . "simples/controllers/Base_imovel_controller.php";

class Imovel extends Base_Imovel_Controller
{
    protected $quantidade_fotos_principais = 3;
    protected $quantidade_sugestoes = 4;

    public function index()
    {
        $data = parent::index();

        $this->load->view('imovel/detalhes', $data);
    }

    public function pesquisar()
    {
        $data = parent::pesquisar();

        $data['complementos'] = $this->imovel_model->todos_complementos();

        $this->load->view('imovel/pesquisa', $data);
    }

    public function buscar()
    {
        $data = parent::buscar();
        $data['status'] = true;

        echo json_encode($data);
    }
}
