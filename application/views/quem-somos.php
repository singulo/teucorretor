<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>

<div class="quem-somos text-center">
    <div class="container">
        <div class="col-md-offset-3 col-md-6">
            <h1>A realização dos teus sonhos <strong><em>é o que nos realiza.</em></strong></h1>
            <br>
            <p>A imobiliária TEUCORRETOR.COM é uma empresa especializada no desenvolvimento de estratégias para a realização de negócios imobiliários. Com uma equipe qualificada e uma visão diferenciada a TEUCORRETOR.COM propõe-se a apresentar produtos e serviços de alta qualidade.</p>
            <br>
            <p>É prioridade para a empresa a transparência e segurança na realização de compra ou venda de imóveis. TEUCORRETOR.COM busca como resultado de seus negócios a satisfação total do cliente visando crescimento através da excelência nos serviços prestados no ramo imobiliário.</p>
            <br>
        </div>
    </div>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    .quem-somos
    {
        padding-top: 30px;
    }

    .quem-somos h1,
    .quem-somos p
    {
        color: #fff;
    }

    .quem-somos h1
    {
        font-size: 23px;
    }

    .quem-somos h3
    {
        font-size: 17px;
    }

    .quem-somos .diretor img
    {
        margin-bottom: 15px;
    }

    .quem-somos hr
    {
        width: 20%;
        border-top: 3px solid #0a2444;
        float: left;
    }
</style>
