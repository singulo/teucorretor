<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->preco_max = 3000000;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->finalidade = array();
    $filtro->dormitorios = NULL;
}
?>
<? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>
<div class="botao-pesquisa">
    <button type="button" class="btn-pesquisar-toggle center-block hidden-md hidden-lg" onclick="$('.painel-pesquisa').slideUp();"><span class="glyphicon glyphicon-search"></span></button>

    <div class="col-md-12 col-xs-12 painel-pesquisa" id="painel-pesquisa">
        <div class="col-xs-12">
            <div class="row">
                <button type="button" class="btn-pesquisar-toggle center-block hidden-md hidden-lg" onclick="$('.painel-pesquisa').slideUp();"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
        <form id="menu-filtro" class="form-filtro" action="<?= base_url('imovel/pesquisar'); ?>">
            <div class="container">
                <div class="col-md-1 input">
                    <select name="id_tipo" class="selectpicker" data-live-search="true" title="Tipo" data-width="100%" multiple>
                        <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                            <option value="<?= $tipo->id; ?>" <? if(select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="col-md-2 input">
                    <select name="cidade" class="selectpicker" data-live-search="true" title="Cidade" data-width="100%" multiple>
                        <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                            <option value="<?= $cidade->cidade; ?>" <? if(select_value($cidade->cidade, $filtro->cidade)) echo 'selected'; ?>><?= $cidade->cidade;?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="col-md-1 input">
                    <select name="dormitorios" class="selectpicker" data-live-search="false" title="Dorm." data-width="100%">
                        <option title="Dorm." value="">0+</option>
                        <?= $pesquisa = $this->config->item('pesquisa'); ?>
                        <? for($i = 1; $i <= $pesquisa['dormitorios']; $i++): ?>
                            <option value="<?= $i; ?>" <? if(select_value($i, $filtro->dormitorios)) echo 'selected'; ?>><?= $i . ($i == $pesquisa['dormitorios'] ? '+' : ''); ?></option>
                        <? endfor; ?>
                    </select>
                </div>
                <div class="col-md-2 input finalidade" style="color:#fff; margin-top: 4px;">
                    <? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>
                    <label>
                        <input type="checkbox" name="finalidade" value="<?= Finalidades::Venda; ?>" <? if(select_value( Finalidades::Venda, $filtro->finalidade)) echo 'checked'; ?>> Venda
                    </label>
                    <label>
                        <input type="checkbox" name="finalidade" value="<?= Finalidades::Aluguel; ?>" <? if(select_value(Finalidades::Aluguel, $filtro->finalidade)) echo 'checked'; ?> style="margin-left: 8px;"> Aluguel
                    </label>
                </div>
                <div class="col-md-2">
                    <? $filtro_valor_min = array('valor' => $filtro->preco_min, 'formatado' => number_format((int)$filtro->preco_min, 2, ',', '.')); ?>
                    <? $filtro_valor_max = array('valor' => $filtro->preco_max, 'formatado' => number_format((int)$filtro->preco_max, 2, ',', '.')); ?>
                    <input type="hidden" name="preco_min" value="<?= $filtro_valor_min['valor']; ?>">
                    <input type="hidden" name="preco_max" value="<?= $filtro_valor_max['valor']; ?>">
                    <input type="text" class="slider-filtro" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= $filtro_valor_min['valor']; ?>, <?= $filtro_valor_max['valor']; ?>]"/>
                    <label class="filtro-valores" style="position: absolute; font-size: 10px;"><small>R$</small> <?= $filtro_valor_min['formatado']; ?> até <small>R$</small><?= $filtro_valor_max['formatado']; ?></label>
                </div>
                <div class="col-md-2 codigo input">
                    <input placeholder="Digite o código" name="id" style="color: black;" value="<?=form_set_value($filtro, 'id'); ?>">
                </div>
                <div class="col-md-2 input pesquisar">
                    <button type="button" class="btn btn-pesquisa btn-sm" onclick="pesquisar(0);">Pesquisar</small><span class="glyphicon glyphicon-search" aria-hidden="true" style="padding-left: 10px;"></span></button>
                </div>
            </div>
        </form>
    </div>
</div>

