<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/filtro-rapido/filtro-rapido.css'); ?>">

<div class="filtro-imovel">
    <div class="container">
        <div class="col-md-3">
            <h2>
                REALIZE <br> <strong>O SEU </strong> SONHO <br> COM <strong>A GENTE</strong>
            </h2>
        </div>
        <form onsubmit="return false;" class="form-filtro col-md-9">
            <div class="col-md-4">
                <select name="tipo" class="selectpicker" data-live-search="true" title="DORMITÓRIO/TIPO">
                    <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                        <option value="<?= $tipo->id; ?>"><?= $tipo->tipo;?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="col-md-4">
                <select name="condominio" class="selectpicker" data-live-search="true" title="CONDOMÍNIO">
                    <? foreach($_SESSION['filial']['condominios_horizontais'] as $condominio) : ?>
                        <option value="<?= $condominio->id; ?>"><?= $condominio->nome;?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="col-md-4">
                <input type="hidden" name="filtro-preco-min">
                <input type="hidden" name="filtro-preco-max">
                <input type="text" class="slider-filtro span2" value="" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[0,3000000]" data-slider-ticks-labels='["R$0", "R$3.000.000"]'/>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-info btn-lg">PROCURAR IMÓVEL</button>
            </div>
        </form>
    </div>
    <nav class="navbar navbar-default filtro-rapido">
        <div class="container text-center">
            <ul>
                <li><button type="button">APARTAMENTO 1 DORM.</button></li>
                <li><button type="button">APARTAMENTO 2 DORM.</button></li>
                <li><button type="button">APARTAMENTO 3 DORM.</button></li>
                <li><button type="button">APARTAMENTO 4 DORM. +</button></li>
                <li><button type="button">CASAS</button></li>
                <li><button type="button">CONDOMÍNIOS</button></li>
            </ul>
        </div>
    </nav>
</div>