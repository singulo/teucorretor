<div class="banner-container">
    <? foreach($_SESSION['filial']['banners'] as $banner) : ?>
        <div class="item" style="height: 100%; background-size: cover; background-image: url(<?= $_SESSION['filial']['banners_uri'] . $banner->id . '.jpg'; ?>);">
            <? if(isset($banner->imovel)) : ?>
                <div class="container">
                    <div class="banner-imovel-container">
                        <div class="row">
                            <div class="col-md-9">
                                <h2 class="banner-imovel-titulo"><?= $_SESSION['filial']['tipos_imoveis'][$banner->imovel->id_tipo]->tipo; ?></h2>
                                <p class="banner-imovel-texto"><em><?= $banner->mensagem; ?></em></p>
                            </div>
                            <div class="col-md-3">
                                <a href="<?= base_url('imovel?id=' . $banner->imovel->id); ?>" title="Ver mais detalhes">
                                    <span class="glyphicon glyphicon-menu-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <? else : ?>
                <div class="col-md-offset-1 col-md-4 txt-banner">
                    <?= $banner->mensagem; ?>
                </div>
            <? endif; ?>
        </div>
    <? endforeach; ?>
</div>

