<div class="rodape col-xs-12">
    <div class="container">
        <div class="info-imobiliaria">
            <div class="col-xs-12 col-md-3">
                <h3>Onde estamos</h3>
                <small> <b><?= $_SESSION['filial']['endereco']; ?></b>
                    <br> <?= $_SESSION['filial']['cidade']; ?> | <?= strtoupper($_SESSION['filial']['estado']); ?>
                    <br>
                    <br> <b><?= $_SESSION['filial']['email_padrao']; ?></b>
                    <br> <?= $_SESSION['filial']['telefone_1']; ?> / <?= $_SESSION['filial']['telefone_2']; ?></small>
            </div>
<!--            <div class="col-xs-12 col-md-3 pull-right">-->
<!--                <h3 class="text-right">FACEBOOK</h3>-->
<!--                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsingulo%2F%3Ffref%3Dts&tabs&width=340&height=70&small_header=true&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="100%" height="70" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>-->
<!--            </div>-->
<!--            <div class="col-xs-12 col-md-3 pull-right">-->
<!--                <h3 class="text-right">FACEBOOK</h3>-->
<!--                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsingulo%2F%3Ffref%3Dts&tabs&width=340&height=70&small_header=true&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="100%" height="70" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>-->
<!--            </div>-->
        </div>
    </div>
</div>
<div class="direitos-simples col-xs-12">
    <div class="container">
        <div class="text-right">
            <a href="http://www.simplesimob.com.br/">
                <img src="<?= base_url('assets/images/simplesimob-logo.png'); ?>" class="img-responsive pull-right" alt="Simples Imob" style="padding-left: 10px; margin-top: -4px;">
            </a>
            <p>Copyright <?= date('Y'); ?> | Todos os direitos reservados, desenvolvido por Singulo Comunicação | Site integrado ao sistema Simples Imob </p>
        </div>
    </div>
</div>