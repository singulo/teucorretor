<div class="condominios-apresentacao-rodape col-xs-12">
    <div class="row">
        <div class="col-md-6 texto text-right">
            <h2>Condomínios Fechados</h2>
            <p>Os imóveis mais luxuosos do litoral norte em<br>condomínios com total infraestrutura e segurança <br> pra sua família.</p>
        </div>
        <div class="col-md-6 condominios">
            <div class="col-md-8 hidden-sm hidden-xs">
                <div class="owl-condominios-apresentacao-rodape">
                <? if(count($_SESSION['filial']['condominios_horizontais']) > 8) : ?>
                    <? foreach(array_chunk($_SESSION['filial']['condominios_horizontais'], 2) as $condominios) : ?>
                        <? if($condominios[0]->fechado || (isset($condominios[1]) && $condominios[1]->fechado)) : ?>
                            <div class="item">
                                <? if($condominios[0]->fechado) : ?>
                                    <a href="<?= base_url('condominio?id=' . $condominios[0]->id); ?>">
                                        <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominios[0]->foto; ?>);"></div>
                                    </a>
                                <? endif; ?>
                                <? if(isset($condominios[1]) && $condominios[1]->fechado) : ?>
                                    <a href="<?= base_url('condominio?id=' . $condominios[1]->id); ?>">
                                        <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominios[1]->foto; ?>);"></div>
                                    </a>
                                <? endif; ?>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                <? else: ?>
                    <? foreach($_SESSION['filial']['condominios_horizontais'] as $condominio) : ?>
                        <? if($condominio->fechado) : ?>
                            <div class="item">
                                <a href="<?= base_url('condominio?id=' . $condominio->id); ?>">
                                    <img src="<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                                </a>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .condominios-apresentacao-rodape
    {
        padding-bottom: 40px;
    }

    .condominios-apresentacao-rodape .condominios
    {
        background-color: #f1f1f1;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    }

    .condominios-apresentacao-rodape .texto
    {
        margin-top: 20px;
        padding-right: 35px;
    }

    .owl-condominios-apresentacao-rodape
    {
        padding-top: 20px;
        padding-bottom: 20px;
    }

    .owl-condominios-apresentacao-rodape .owl-controls
    {
        display: none;
    }

    .owl-condominios-apresentacao-rodape .item .imagem
    {
        height: 100px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100%;
        background-color: #fff;
    }
</style>

<script>
    $(document).ready(function(){
        $('.owl-condominios-apresentacao-rodape').owlCarousel({
            autoPlay: 3000,
            items: 4,
            loop: true,
            margin: 0,
            //pagination: false
            navegation: false,
            responsive : {
                0:{
                    items: 1,
                },
                768:{
                    items: 2,
                },
                1000: {
                    items: 4,
                }
            }
        });
    });
</script>