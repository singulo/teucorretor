<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/templates/menu/menu.css'); ?>">

<div class="menu-superior hidden-xs hidden-sm">
    <div class="container">
        <div class="col-md-5 pull-right">
            <a href="tel:+55 <?= $_SESSION['filial']['telefone_1']; ?>"><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><small>+55 </small><?= $_SESSION['filial']['telefone_1']; ?></a>
            <a href="tel:+55 <?= $_SESSION['filial']['telefone_2']; ?>"><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><small>+55 </small> <?= $_SESSION['filial']['telefone_2']; ?></a>
            <? if($this->session->has_userdata('usuario')) : ?>
                <a href="#modal-dados" data-toggle="modal"><?= $this->session->userdata('usuario')->nome; ?></a>
            <? else : ?>
                <a href="#modal-login" data-toggle="modal"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> login/cadastro</a>
            <? endif; ?>
        </div>
    </div>
</div>
<div class="menu-imovel hidden-xs hidden-sm">
    <div class="container">
        <div class="row menu-items">
            <div class="col-md-3">
                <a href="<?= base_url(); ?>"><img src="<?= base_url('assets/images/logo-menu.png'); ?>"></a>
            </div>
            <div class="col-md-7 pull-right menu-principal">
                <ul class="text-right">
                    <li><a href="<?= base_url('quem-somos'); ?>">QUEM SOMOS</a></li>
                    <li><a href="<?= base_url('imovel/pesquisar'); ?>">IMÓVEIS</a></li>
<!--                <li><a href="<?/*= base_url('condominio/todos_fechados'); */?>">CONDOMÍNIOS</a></li>
                    <li><a href="<?/*= base_url('condominio/lancamentos'); */?>">LANÇAMENTOS</a></li>
                    <li><a href="<?/*= base_url('contato'); */?>">VENDA SEU IMÓVEL</a></li>
-->                 <li><a href="<?= base_url('contato'); ?>">CONTATO</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="visible-sm visible-xs menu-mobile">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url('home'); ?>"><img src="<?= base_url('assets/images/logo-menu.png') ?>" alt="logo-preferencial" class="logo-teucorretor"></a>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?= base_url('quem-somos'); ?>">QUEM SOMOS</a></li>
                    <li><a href="<?= base_url('imovel/pesquisar'); ?>">IMÓVEIS</a></li>
                    <!--<li><a href="<?/*= base_url('condominio/todos_fechados'); */?>">CONDOMÍNIOS</a></li>
                    <li><a href="<?/*= base_url('condominio/lancamentos'); */?>">LANÇAMENTOS</a></li>
                    <li><a href="<?/*= base_url('contato'); */?>">VENDA SEU IMÓVEL</a></li>
-->                 <li><a href="<?= base_url('contato'); ?>">CONTATO</a></li>
                    <li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <a href="#modal-dados" data-toggle="modal"><?= $this->session->userdata('usuario')->nome; ?></a>
                        <? else : ?>
                            <a href="#modal-login" data-toggle="modal"><i class="glyphicon glyphicon-user" aria-hidden="true"></i> login/cadastro</a>
                        <? endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>


<? $this->load->view('cliente/login'); ?>
<? $this->load->view('cliente/cadastrar'); ?>
<? if($this->session->has_userdata('usuario')) :?>
    <? $this->load->view('cliente/dados'); ?>
<?endif; ?>
