<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-principal'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/home/home.css'); ?>">

<div class="container msg-bem-vindo">
    <div class="col-md-8">
        <p>A imobiliária TEUCORRETOR.COM é uma empresa especializada no desenvolvimento de estratégias para a realização de negócios imobiliários. Com uma equipe qualificada e uma visão diferenciada a TEUCORRETOR.COM propõe-se a apresentar produtos e serviços de alta qualidade.</p>
    </div>
    <div class="col-md-4">
        <h3>Seja bem-vindo</h3>
        <p>Confira a seguir algumas ofertas que separamos especialmente para você.</p>
    </div>
</div>

<? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>

<!-- DESTAQUES -->
<? foreach($imoveis_destaque as $destaque) : ?>
    <div class="imoveis-destaques">
        <div class="container">
            <h3 class="text-center"><i><?= $destaque['titulo']; ?></i></h3><br>
            <div class="col-md-12">
                <div class="owl-destaques">
                    <? foreach($destaque['imoveis'] as $imovel) : ?>
                        <div class="item imovel">
                            <a href="<?= base_url('imovel?id=' . $imovel->id); ?>">
                                <span class="finalidade"><?= Finalidades::toString($imovel->finalidade); ?></span>
                                <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="tipo"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></h3>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="descricao"><?= ellipsize($imovel->descricao, 100); ?></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><?= format_valor_miniatura($imovel, 'R$ '); ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<? endforeach; ?>


<? if(! is_null($condominio_destaque)) : ?>
    <div class="col-xs-12 imovel-destaque-maior">
        <div class="row descricao">
            <div class="col-md-6">
                <div class="col-md-7 pull-right">
                    <h2><?= $condominio_destaque->nome; ?></h2>
                    <div class="col-xs-12">
                        <hr>
                    </div>
                    <p><?= $condominio_destaque->descricao; ?></p>
                    <h3>CONDOMÍNIO <?= $condominio_destaque->fechado ? 'FECHADO' : 'ABERTO'; ?></h3>
                </div>
            </div>
            <div class="foto hidden-xs hidden-sm">
                <div class="col-md-6">
                    <div class="row">
                        <a href="<?= base_url('condominio?id=' . $condominio_destaque->id); ?>">
                            <img class="img-responsive center-block" src="<?= $_SESSION['filial']['fotos_condominios'] . $condominio_destaque->foto; ?>" alt="<?= 'Condomínio Fechado ' . $condominio_destaque->nome; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>


<? if(count($_SESSION['filial']['condominios_horizontais']) > 0) : ?>
    <? $this->load->view('templates/condominio/apresentacao-rodape'); ?>
<? endif; ?>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script type="text/jscript" src="<?= base_url('assets/pages/home/home.js'); ?>"></script>

