<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/contato/contato.css'); ?>">

<div class="mapa-contato">
    <div class="col-md-6 mapa">
        <div class="row">
            <div id="map-canvas" data-marker-none="true" data-lat-lng="<?= $_SESSION['filial']['latitude_longitude']; ?>" style="width: 100%; height: 500px;"></div>
        </div>
    </div>
    <div class="col-md-6 contato">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <em>
                        <h1>Fale com a gente</h1>
                        <p>A Equipe de TEU CORRETOR, quer te ajudar a encontrar o teu próximo imóvel na práia. Para dúvidas ou sugestões informe a seguir teus dados que entraremos em contato.</p>
                    </em>
                </div>
            </div>
        </div>

        <form id="form-contato" onsubmit="return false;">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" name="nome" class="form-control" placeholder="Qual é o seu nome?">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Informe seu email">
                </div>
                <div class="form-group">
                    <input type="text" name="telefone" class="form-control telefone" placeholder="Número para contato?">
                </div>
                <div class="form-group">
                    <input type="text" name="assunto" class="form-control" placeholder="Qual é o assunto?">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <textarea name="mensagem" class="form-control" rows="9" placeholder="Digite aqui o motivo do seu contato..."></textarea>
                </div>

                <button type="button" class="btn btn-danger pull-right btn-enviar" data-loading-text="Aguarde..." onclick="cliente_enviar_novo_contato();">ENVIAR</button>
            </div>
        </form>
    </div>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<!--MAPA-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIN1JY_p-QMDCkL4Dm9Qo2HqByRzeDYYg"></script>
<script src="<?= base_url('assets/js/gmap.js'); ?>"></script>