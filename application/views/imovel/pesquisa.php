<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/imovel/pesquisa.css'); ?>" />

<div class="container">
    <div class="imoveis col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center"><em id="msg-resultados-encontrados"><strong>Busque</strong> <br> <small>ofertas em nosso site.</small></em></h3>
            </div>
        </div>

        <hr>

        <div class="row" id="imoveis-encontrados">
            <div class="col-md-4" id="imovel-visualizacao" style="display: none;">
                <div class="imovel">
                    <a href="#" class="imovel-url">
                        <span class="finalidade">Venda</span>
                        <img src="http://placehold.it/350x350" class="imovel-img" onerror="this.src='http://placehold.it/350x350'">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="tipo"></h3>
                            </div>
                            <div class="col-md-6 ">
                                <p class="descricao"></p>
                            </div>
                            <div class="col-md-12">
                                <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><span class="imovel-valor"></span></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="center-block" style="display: table;">
            <div id="pagination" class="pagination-holder clearfix">
                <div id="light-pagination" class="pagination light-theme simple-pagination">
                </div>
            </div>
        </div>
    </div>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script>

    var filtro = <?= json_encode($filtro); ?>;
</script>

