<? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>

<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/tabs/css/tabs.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/tabs/css/tabstyles.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/pages/empreendimento/empreendimento.css'); ?>" />

<? $this->load->view('imovel/contato-interesse'); ?>

<!--<div class="hidden-xs hidden-sm">-->
<!--    <div id="imagens-principais-carousel">-->
<!--        --><?// foreach($imovel->fotos_principais as $foto) : ?>
<!--            <div class="item">-->
<!--                <img src="--><?//= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?><!--"  height="580px">-->
<!--            </div>-->
<!--        --><?// endforeach; ?>
<!--    </div>-->
<!--    <div class="hidden-xs hidden-md owl-carousel-navigation">-->
<!--        <a class="prev"><img src="--><?//= base_url('assets/images/arrow-prev.png'); ?><!--"></a>-->
<!--        <a class="next"><img src="--><?//= base_url('assets/images/arrow-next.png'); ?><!--"></a>-->
<!--    </div>-->
<!--</div>-->

<div class="container">
    <div class="col-md-7 imovel-midias">
        <? if($imovel->fotos_principais[0]) : ?>
            <img class="img-responsive" width="100%" src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>">
        <? endif; ?>
        <? if(!$this->session->has_userdata('usuario')) : ?>
            <div class="aviso-para-ver-fotos text-left">
                <h4>Este imóvel contem <strong><?= $total_fotos; ?> fotos</strong> e outras <strong>mídias</strong></h4>
                <h5>Tenha acesso as mídias de <strong>todos imóveis</strong>, apenas faça seu cadastro.</h5>
                <button class="btn btn-danger" data-toggle="modal" data-target="#modal-login">Entre/Cadastre-se</button>
            </div>
        <? else : ?>
            <section>

                <h4>MÍDIAS DO IMÓVEL</h4>

                <div class="tabs tabs-style-linetriangle">
                    <nav>
                        <ul>
                            <li>
                                <a href="#section-fotos">
                                    <span>
                                        <img src="<?= base_url('assets/images/icon-photo.png'); ?>"><br> <small>Fotos  (<?= count($imovel->fotos['normais']);?>)</small>
                                    </span>
                                </a>
                            </li>
                            <? if(count($imovel->fotos['plantas']) > 0) : ?>
                                <li>
                                    <a href="#section-plantas">
                                        <span>
                                            <img src="<?= base_url('assets/images/icon-metragem.png'); ?>"><br> <small>Plantas (<?= count($imovel->fotos['plantas']);?>)</small>
                                        </span>
                                    </a>
                                </li>
                            <? endif; ?>
                            <? if(count($imovel->videos) > 0) : ?>
                                <li>
                                    <a href="#section-videos">
                                        <span>
                                            <img src="<?= base_url('assets/images/icon-youtube.png'); ?>"><br> <small>Vídeos (<?= count($imovel->videos);?>)</small>
                                        </span>
                                    </a>
                                </li>
                            <? endif; ?>
                        </ul>
                    </nav>
                    <div class="content-wrap">
                        <section id="section-fotos">
                            <? foreach($imovel->fotos['normais'] as $foto) : ?>
                                <div class="col-md-3 fotos-galeria">
                                    <a rel="galleria-fotos" href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" title="<?= $foto->legenda; ?>">
                                        <img class="img-responsive" style="max-width:180%;" src="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" >
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </section>
                        <section id="section-plantas">
                            <? foreach($imovel->fotos['plantas'] as $foto) : ?>
                                <div class="col-md-3 fotos-galeria">
                                    <a rel="galleria-plantas" href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" title="<?= $foto->legenda; ?>">
                                        <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" >
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </section>
                        <section id="section-videos">
                            <? foreach($imovel->videos as $video) : ?>
                                <div class="col-md-4">
                                    <iframe width="100%" height="150" src="https://www.youtube.com/embed/<?= $video->id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <? endforeach; ?>
                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        <? endif; ?>
    </div>
    <div class="col-md-5 imovel-descricao">
        <h4 class="pull-left">DESCRIÇÃO IMÓVEL</h4> <small class="pull-right">CÓDIGO: <?= $imovel->id; ?></small>
        <?if(isset($imovel->condominio)): ?>
            <br><a href="<?= base_url('condominio?id=' . $imovel->condominio->id); ?>"><small class="pull-right">Condomínio: <?= $imovel->condominio->nome; ?></small></a>
        <?endif; ?>
        <div class="col-xs-12 no-padding">
            <div class="row">
                <ul class="nav nav-pills text-center">
                    <? if($imovel->area_total > 0): ?>
                        <li>
                            <span><?= $imovel->area_total; ?>m²</span><br><small> ÁREA TOTAL</small>
                        </li>
                    <? endif; ?>
                    <li>
                        <span><?= $imovel->dormitorios; ?></span><br><small> DOMITÓRIOS</small>
                    </li>
                    <li>
                        <span><?= $imovel->suites; ?></span><br><small> SUÍTES</small>
                    </li>
                    <li>
                        <span><?= $imovel->banheiros; ?></span><br><small> BANHEIROS</small>
                    </li>
                    <li>
                        <span><?= $imovel->garagem; ?></span><br><small> VAGAS</small>
                    </li>
                </ul>

                <p><?= $imovel->descricao; ?></p>

                <div class="col-xs-12 imovel-valor">
                    <div class="row">
                        <div class="col-md-7">
                            <h1><?= format_valor_miniatura($imovel, '<small>R$</small>'); ?></h1>
                        </div>
                        <div class="col-md-5">
                            <button type="button" class="btn btn-danger btn-interesse">FAZER PROPOSTA</button>
                        </div>
                    </div>
                </div>

                <div class="imovel-complementos">
                    <? if($imovel->complementos) : ?>
                        <h4>AQUI VOCÊ VAI ENCONTRAR</h4>
                        <? foreach($imovel->complementos as $complemento) : ?>
                            <span><?= $complemento->complemento; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="imoveis-similares">
    <div class="container">

        <div class="col-xs-12">
            <h2><em>Imóveis similares</em><br><hr></h2>
        </div>

        <? foreach($imoveis_similares as $imovel) : ?>
            <div class="imovel col-xs-12 col-md-3">
                <a href="<?= base_url('imovel?id=' . $imovel->id); ?>">
                    <span class="finalidade"><?= Finalidades::toString($imovel->finalidade); ?></span>
                    <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="tipo"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></h3>
                        </div>
                        <div class="col-md-6">
                            <p class="descricao"><?= ellipsize($imovel->descricao, 100); ?></p>
                        </div>
                        <div class="col-md-12">
                            <p class="valor"><i class="glyphicon glyphicon-triangle-right"></i><?= format_valor_miniatura($imovel, 'R$ '); ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <? endforeach; ?>
    </div>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script src="<?= base_url('assets/plugins/tabs/js/cbpFWTabs.js'); ?>"></script>

<script>
    (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });

    })();

    $(document).ready(function(){
        $('#imagens-principais-carousel').owlCarousel({
            center: true,
            autoplay: true,
            autoWidth: true,
            items: 1,
            loop: true,
            margin: 0,
            responsive:{
                1200:{
                    items: 1.47
                }
            }
        });

        /*$('#imagens-imovel-carousel').owlCarousel({
            items: 4,
        });*/

        $(".owl-carousel-navigation .next").click(function(){
            $('#imagens-principais-carousel').trigger('next.owl.carousel');
        });
        $(".owl-carousel-navigation .prev").click(function(){
            $('#imagens-principais-carousel').trigger('prev.owl.carousel');
        });

        $('#section-fotos a').fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
        });

        $('#section-plantas a').fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
        });
    });
</script>