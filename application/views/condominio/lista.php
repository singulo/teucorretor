<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-principal'); ?>

<div class="container">

    <h1><?= $titulo; ?></h1>
    <p><em><?= $texto; ?></em></p>

    <div class="condominios row">

        <? $this->load->library('simples/CondominioTipos'); ?>
        <? foreach($condominios as $condominio) : ?>
            <div class="col-md-4">
                <div class="condominio">
                    <a href="<?= base_url('condominio?id=' . $condominio->id); ?>">
                        <span class="finalidade"><?= CondominioTipos::toString($condominio->condominio_tipo); ?></span>
                        <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>" alt="<?= 'Condomínio Fechado ' . $condominio->nome; ?>" onError="this.src = '<?= base_url('assets/images/imovel-sem-foto.jpg'); ?>'">
                        <h3><?= $condominio->nome; ?></h3>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>

<? $this->load->view('templates/rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    .condominios
    {
        padding-top: 30px;
        margin-bottom: 30px;
    }

    .condominios .condominio
    {
        border: 1px solid #e2e2e2;
        margin-bottom: 30px;
    }

    .condominios .condominio h3
    {
        padding-left: 15px;
        color: black;
        font-style: italic;
        text-align: center;
    }

    .condominios .condominio img
    {
        height: 250px;
    }
</style>

