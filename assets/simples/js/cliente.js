var on_cliente_entrar = {
    successCallback : [],
    failureCallback : [],
    errorCallback : []
};

function cliente_verifica_email(email, successCallback, failureCallback, errorCallback, completeCallback)
{
    //Fazer requisicao, checar o email e setar a sessao
    $.ajax({
        url : $('#base_url').val() + 'cliente/login',
        type : 'POST',
        data : { email : email},
        dataType: "json",
        success : function(data) {

            if(data.status == true)
            {//já esta cadastrado
                $.each( on_cliente_entrar.successCallback, function( key, action ) {
                    action(data.usuario);
                });

                successCallback(data.usuario);
            }
            else
            {
                $.each( on_cliente_entrar.failureCallback, function( key, action ) {
                    action(data);
                });

                failureCallback(data);
            }
        },
        error : function(jqXhr, textStatus, errorThrown)
        {
            $.each( on_cliente_entrar.errorCallback, function( key, action ) {
                action(data);
            });

            errorCallback(jqXhr, textStatus, errorThrown);
        },
        complete: function () {
            completeCallback();
        }
    });
}

function cliente_registrar(cliente, simpleRequest)
{
    ajaxPost(
        cliente,
        $('#base_url').val() + 'cliente/novo',
        simpleRequest
    );
}

function cliente_atualizar_dados(cliente, simpleRequest)
{
    ajaxPost(
        cliente,
        $('#base_url').val() + 'cliente/atualizar',
        simpleRequest
    );
}

function cliente_contato_novo(contato, simpleRequest)
{
    ajaxPost(
        contato,
        $('#base_url').val() + 'contato/novo',
        simpleRequest
    );
}

function cliente_interesse_em_imovel(cod_imovel, obs, simpleRequest)
{
    ajaxPost(
        {cod_imovel : cod_imovel, obs : obs},
        $('#base_url').val() + 'contato/interesse_imovel',
        simpleRequest
    );
}

function obter_cidades(form, cidadeSelected)
{
    var select_cidade = $($(form).find('select[name="cidade"]')[0]);
    select_cidade.find('.bs-title-option').text('Carregando...');
    select_cidade.find('option:not(.bs-title-option)').remove();
    select_cidade.prop('disabled', true);
    select_cidade.selectpicker('refresh');

    obter_cidades_pela_uf( { uf: $(form).find('select[name="uf"]').val() }, {
        successCallback: function(data)
        {
            $.each(data.cidades, function(index, cidade){
                select_cidade.append($('<option>', {
                    value: cidade.nome.toUpperCase(),
                    text: cidade.nome
                }));
            });

            select_cidade.find('.bs-title-option').text('Cidade');
            select_cidade.prop('disabled', false);

            if(cidadeSelected != undefined)
                select_cidade.val(cidadeSelected);

            if(cidadeSelected == "")
                select_cidade.prop('disabled', true);

            select_cidade.selectpicker('refresh');
        },
        failureCallback: function(data){
            alertify.error('Ocorreu um erro ao carregar as cidades. Por favor tente mais tarde!');
        },
        errorCallback: function(){
            alertify.error('Ocorreu um erro ao carregar as cidades.');
        }
    })
}