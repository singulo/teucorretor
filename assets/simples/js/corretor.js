function obter_todos_corretores(successCallback, failCallback)
{
    $.ajax({
        url: $('#base_url').val() + 'admin/corretor/lista?todos=sim' ,
        type: 'GET',
        dataType: "json",
        success: function (data) {
            successCallback(data);
        },
        error: function (jqXhr, textStatus, errorThrown) {
            failCallback(jqXhr, textStatus, errorThrown);
        }
    });
}

function obter_corretores_randomico(successCallback, failCallback)
{
    $.ajax({
        url: $('#base_url').val() + 'corretor/randomico_ou_vinculado' ,
        type: 'GET',
        dataType: "json",
        success: function (data) {
            successCallback(data);
        },
        error: function (jqXhr, textStatus, errorThrown) {
            failCallback(jqXhr, textStatus, errorThrown);
        }
    });
}