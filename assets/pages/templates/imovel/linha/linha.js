$(document).ready(function(){

    $('.owl-imoveis-linha').owlCarousel({
        autoPlay: 3000,
        items: 4,
        loop: true,
        margin: 10,
        pagination: true,
        responsive : {
            0 : {
                items: 1
            },
            480 : {
                items: 2
            },
            1024 : {
                items: 4
            },
        }
    });

    $(".imoveis-linha button.next").click(function(){
        $('.owl-imoveis-linha').trigger('next.owl.carousel');
    });

    $(".imoveis-linha button.prev").click(function(){
        $('.owl-imoveis-linha').trigger('prev.owl.carousel');
    });
});
