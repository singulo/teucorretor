$('.owl-destaques').owlCarousel({
    autoplay: true,
    items: 3,
    loop: false,
    margin: 25,
    //pagination: true
    responsive : {
        0:{
            items: 1,
        },
        768:{
            items: 2,
        },
        1000: {
            items: 3,
        }
    }
});