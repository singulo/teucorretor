var maxResultado = 12;

var imovelVisualizacao = $('#imovel-visualizacao').clone();

/*if(window.filtro != undefined)
{
    $("select.selectpicker[name='id_tipo']").selectpicker('val', filtro.tipo);
    $("select.selectpicker[name='dormitorios']").selectpicker('val', filtro.dormitorios);
    $("select.selectpicker[name='finalidade']").selectpicker('val', filtro.finalidade);
}*/

$(document).ready(function(){
    if(window.location.pathname == '/novo/imovel/pesquisar')
        pesquisar(0);

});

function pesquisar(pagina)
{
    if(window.location.pathname != '/novo/imovel/pesquisar')
    {
        $('#menu-filtro').submit();
    }
    else {

        var msg_encontrados = '';
        if(pagina > 0)
            msg_encontrados = $('#msg-resultados-encontrados').html();

        $('#msg-resultados-encontrados').html('Buscando <strong>imóveis</strong>, aguarde ...');

        //$('#imoveis-encontrados').html('');
        $('#imoveis-encontrados').children().fadeOut(800, function(){
            $(this).remove();
        });

        filtro = {};

        filtro.filtro = $('#menu-filtro').jsonify();
        filtro.pagina = pagina;
        filtro.limite = maxResultado;

        ajaxPost(
            filtro,
            $('#base_url').val() + 'imovel/buscar',
            {
                successCallback: function (data) {
                    $.each(data.imoveis, function (key, imovel) {
                        $('#imoveis-encontrados').append(montar_imovel(imovel));
                    });

                    if (data.total != undefined && data.total > 0)
                    {
                        $('#msg-resultados-encontrados').html('<strong>Encontramos ' + data.total + '</strong> <br> <small>ofertas em nosso site.</small>');
                        $('#pagination').pagination('updateItems', data.total);
                    }
                    else if(data.total != undefined)
                    {
                        $('#msg-resultados-encontrados').html('Nada foi encontrado.');
                        $('#pagination').pagination('updateItems', 0);
                    }
                    else
                        $('#msg-resultados-encontrados').html(msg_encontrados);
                },
                failureCallback: function (data) {
                    alertify.error('Ocorreu um erro ao obter os imóveis.')
                },
                errorCallback: function (jqXhr, textStatus, errorThrown) {
                    alertify.error('Ocorreu um erro ao obter os imóveis. Tente novamente mais tarde.')
                },
                completeCallback: function () {
                    if(pagina > 0)
                    {
                        $('html, body').animate({
                            scrollTop: $("#imoveis-encontrados").offset().top - 120
                        }, 1000);
                    }
                }
            }
        );
    }
}

function montar_imovel(imovel)
{
    novaDiv = imovelVisualizacao.clone();

    novaDiv.find('img.imovel-img').attr('src', $('#filial_fotos_imoveis').val() + imovel.foto);
    //novaDiv.find('.imovel-img img').attr('src', $('#filial_fotos_imoveis').val() + imovel.foto + ".jpg");

    novaDiv.find('.imovel-url').attr('href', $('#base_url').val() + 'imovel?id=' + imovel.id);

    novaDiv.find('.tipo').text(imoveis_tipos[imovel.id_tipo].tipo);
    novaDiv.find('.finalidade').text(finalidades[imovel.finalidade]);

    if(imovel.descricao.length > 95)
        imovel.descricao = imovel.descricao.substr(0, 92) + '...';

    novaDiv.find('.descricao').text(imovel.descricao);

    novaDiv.find('.imovel-valor').html(exibir_valor_imovel(imovel, 'R$'));

    novaDiv.show();

    return novaDiv;
}

$(function() {
    $('#pagination').pagination({
        items: 0,
        itemsOnPage: maxResultado,
        cssStyle: 'light-theme',
        nextText: 'Próximo <i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i> Anterior',
        onPageClick : function(pageNumber){
            pesquisar((pageNumber-1), maxResultado);
        }
    });
});