$('.selectpicker').selectpicker();

$('.telefone').mask('(99)9999-9999?9');
$(".valor-slider").slider({});


function isMobile() { return ('ontouchstart' in document.documentElement); }

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    $('.selectpicker').addClass('mobile-device');

    document.getElementById('painel-pesquisa').style.display = 'none';

    $('.btn-pesquisar-toggle').on('click touchstart', function(){
        $('.painel-pesquisa').slideToggle();
    });
}

function obter_cidades_pela_uf(uf, simpleRequest)
{
    ajaxPost(
        uf,
        $('#base_url').val() + 'cidade/obter_cidade_pela_uf',
        simpleRequest
    );
}

$("#modal-dados").on('show.bs.modal', function () {
    if($('#form-dados select[name=uf]').val() != undefined && $('#form-dados select[name=uf]').val() != '')
        obter_cidades($('#form-dados'), $('#form-dados select[name=cidade]').data('cidade-default'));
});