$(document).ready(function(){
    // VALOR SLIDER
    $('form.form-filtro .slider-filtro').slider({
        formatter: function(value) {

            if(value[0] != undefined)
                atualiza_label_no_slider_e_formatted(value[0], value[1]);

            if(value[1] == 3000000)
                return exibir_valor_imovel(value[0], '', '0,00') + ' : ' + exibir_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return exibir_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + exibir_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $("form.form-filtro .slider-filtro").on("slide", function(slideEvt) {
        $('form.form-filtro input[name="preco_min"]').val(slideEvt.value[0]);
        $('form.form-filtro input[name="preco_max"]').val(slideEvt.value[1]);

        atualiza_label_no_slider_e_formatted(slideEvt.value[0], slideEvt.value[1]);
    });
});

function atualiza_label_no_slider_e_formatted(valMin, valMax)
{
    var mostrar_mais = '';
    if(valMax == 3000000)
        mostrar_mais = '+';

    $('form.form-filtro .filtro-valores').html('<small>R$</small>' + formata_valor_imovel(valMin, '', '0,00') + ' até ' + '<small>R$</small>' + formata_valor_imovel(valMax, '', '0,00') + mostrar_mais);
}