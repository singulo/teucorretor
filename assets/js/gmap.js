jQuery(document).ready(function($){

    var marker;
    var map;

    function initialize() {

        var elementMap = document.getElementById('map-canvas');

        var latLng = ($(elementMap).data('lat-lng')).replace(' ', '').split(',');
        var Latlng = new google.maps.LatLng(latLng[0],latLng[1]);
        var centerLatLng = Latlng;

        if($(elementMap).data('center') != undefined)
        {
            centerLatLng = ($(elementMap).data('center')).replace(' ', '').split(',');
            centerLatLng = new google.maps.LatLng(centerLatLng[0],centerLatLng[1]);
        }

        var zoom = 16;
        if($(elementMap).data('map-zoom'))
            zoom = $(elementMap).data('map-zoom')

        var mapOptions = {
            zoom: zoom,
            center: centerLatLng,
            scrollwheel: false
        };
        map = new google.maps.Map(elementMap,
            mapOptions);

        var image = $('#base_url').val() + 'assets/images/mapicon.png';
        if($(elementMap).data('marker-none'))
            image = null;

        marker = new google.maps.Marker({
            position: Latlng,
            map: map,
            icon: image,
            animation: google.maps.Animation.DROP,
            draggable: false
        });

        var radius = 200;

        if($(elementMap).data('radius-size'))
            radius = $(elementMap).data('radius-size');

        if($(elementMap).data('circle-around'))
        {
            // Add circle overlay and bind to marker
            var circle = new google.maps.Circle({
                map: map,
                radius: radius,    // 10 miles in metres
                fillColor: '#e55050',
                strokeWeight: 0 // largura da borda
            });
            circle.bindTo('center', marker, 'position');
        }

        google.maps.event.addListener(marker, 'click', toggleBounce);
    }

    function toggleBounce() {
        if (marker.getAnimation() != null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});