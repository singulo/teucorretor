$(document).ready(function() {

    $("#form-dados").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            nome: {
                required: true,
                minlength: 5
            }
        }
    });

});

function cliente_salvar_dados()
{
    if(!$("#form-dados").valid()) return;

    var $btn = $('.btn-salvar').button('loading');

    var dados = $("#form-dados").jsonify();

    cliente_atualizar_dados(
        dados,
        {
            successCallback: function(data){
                alertify.success('Seus dados foram atualizados!');
            },
            failureCallback: function(data){
                alertify.error('Por favor certifique-se de ter editado pelo menos um dos campo.');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao atualizar seus dados.');
            },
            completeCallback: function(){
                $btn.button('reset');
            }
        });
}